import serial

def auto(ser):
    ser.write("3".encode())
    reply = ser.readline().decode() # read a '\n' terminated line
    print("- {}".format(reply))
    return reply

def pump_on(ser):
    ser.write("2".encode())
    reply = ser.readline().decode() # read a '\n' terminated line
    print("- {}".format(reply))
    return reply

def pump_off(ser):
    ser.write("1".encode())
    reply = ser.readline().decode() # read a '\n' terminated line
    print("- {}".format(reply))
    return reply

def connect(port, baudrate):
    return serial.Serial(port, baudrate, timeout=1)


def raiseError(ser, error):
    ser.write("error=" + str(error) + "\n".encode())
    reply = ser.readline().decode() # read a '\n' terminated line
    print("- {}".format(reply))
    return reply
