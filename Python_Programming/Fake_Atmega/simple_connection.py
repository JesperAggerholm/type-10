import serial
import time

serial_cfg = {  "dev": "./fake_atmega",
                "baud": 9600 }

if __name__ == "__main__":
    print( "Running port {}".format( serial_cfg['dev'] ) )

    with serial.Serial(serial_cfg['dev'], serial_cfg['baud'], timeout=1) as ser:
        try:
            print("led1on")
            ser.write( "led1on\n".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print("- {}".format(reply))

            print("zzzz")
            time.sleep(1)

            print("led1off")
            ser.write( "led1off\n".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print( "- {}".format(reply))
            
            print("zzzz")
            time.sleep(1)
            
            print("analog1")
            ser.write( "analog1\n".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print( "- {}".format(reply))
            
            print("zzzz")
            time.sleep(1)
            
            print("analog2")
            ser.write( "analog2\n".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print( "- {}".format(reply))
            
            print("zzzz")
            time.sleep(1)
            
            print("analog3")
            ser.write( "analog3\n".encode() )
            reply = ser.readline().decode()   # read a '\n' terminated line
            print( "- {}".format(reply))
            
            print("zzzz")
            time.sleep(1)

        except KeyboardInterrupt:
            print('interrupted!')