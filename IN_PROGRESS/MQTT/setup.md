# Instalation

setup on the raspberry pi, start up by typing the following commands into a terminal. 

`sudo apt-get install -y mosquitto mosquitto-clients`

after the installation, the Mosquitto server is started automatically. you can open a 
subscriber in the channel "test_channel" waiting for messages:

`mosquitto_sub -h localhost -v -t test_channel`


