import cv2, time, pandas
from datetime import datetime

first_frame = None
status_list = [None,None]
times = []
df = pandas.DataFrame(columns=["Start", "End"])
#Selecting the capture device (-1) to select any avaible device.
video = cv2.VideoCapture(0)

a = 0

while True:
    a = a+1
    check, frame = video.read()
    status = 0
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray,(21,21),0)

    if first_frame is None:
        first_frame = gray
        continue

    #Configuring the three different kinds of thresholds.
    delta_frame = cv2.absdiff(first_frame, gray)
    thresh_frame = cv2.threshold(delta_frame, 30, 255, cv2.THRESH_BINARY)[1]
    thresh_frame = cv2.dilate(thresh_frame, None, iterations = 2)

    #(cnts,_) for python2, (_,cnts,_) for python3.
    (cnts,_)=cv2.findContours(thresh_frame.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    #Filters out the contours, within defined area.(The larger the number, the bigger the object to detect)
    for contours in cnts:
        if cv2.contourArea(contours) < 10000:
            continue
        status = 1
        #Draws a rectangle, tracking the object.
        (x, y, w, h) = cv2.boundingRect(contours)
        #Defining the proportions and color of the rectangle.
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0,255,9), 3)

    status_list.append(status)

    if status_list[-1]==1 and status_list[-2]==0:
        times.append(datetime.now())

    if status_list[-1]==0 and status_list[-2]==1:
        times.append(datetime.now())

    #Displaying the different frames.
    cv2.imshow("Grayframe", gray)
    cv2.imshow("Deltaframe", delta_frame)
    cv2.imshow("Threshhold Frame", thresh_frame)
    cv2.imshow("Color Frame", frame)

    key = cv2.waitKey(1)

    #For printing the captured values detected.
    #print(gray)
    #print(delta_frame)

    # Quits the program if you press the (q) key.
    if key==ord('q'):
        if status == 1:
            times.append(datetime.now())
        break

for i in range(0, len(times),2):
        df=df.append({"start":times[i],"End":times[i+1]},ignore_index = True)

df.to_csv("Times.csv")

#Releases the webcap, and destroys the window.
video.release()
cv2.destroyAllWindows
