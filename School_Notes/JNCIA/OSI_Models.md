# OSI Reference Model

The OSI(Open Systems Interconnect) Reference Model, defines a seven layer model of data communication. 
It's widely accepted as a basis for the understanding of how a network protocol stack should operate and as a reference tool for comparing network stack implementation.

Each layer provides a set of functions to the layer above, and in turn, relies on the functions provided by the layer below.
Although messages can only pass vertically through the stack from layer to layer, from a logical point of view, each layer communicates directly with it's peer layer on the other nodes.
The layers are the following.

- **Application Layer:** Network applications such as terminal emulation and file transfer.
- **Presentation Layer:** Formatting of data and encryption.
- **Session Layer:** Establishment and maintenance of sessions.
- **Transportation Later:** Provision of reliable and unreliable end-to-end delivery(TCP/UDP).
- **Network Layer:** Packet delivery, including routing(IP, ICMP, ARP).
- **Data link Layer:** Framing of units of information and error checking(Ethernet, Frame Relay, x.25, HLDC, SNA/SDLC, ATM).
- **Physical Layer:** Transmission of bits on the physical hardware(wired or wireless).

---

![alt text](https://gitlab.com/JesperAggerholm/type-10/raw/master/School_Notes/JNCIA/Images/OSI-Model.png "Logo Title Text 1")