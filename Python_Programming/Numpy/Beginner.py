import numpy as np
import time, sys
# Single dimensioneal Numpy Array:

a = np.array([1,2,3])
print(a)
# Multi-dimentional Array:

b = np.array([(1,2,3),(4,5,6)])
print(b)

# The below output shows the memeory allocated by list(denoted by s) is 28000.
# Whereas the memory allocated by the numpy array is just 4000, from this we can conclude
# that there is a major difference between the two.

s = range(1000)
print(sys.getsizeof(5)*len(s))

d = np.arange(1000)
print(d.size*d.itemsize)

# How is a numpy array faster and more convenient when compared to list.

size = 1000000
l1 = range(size)
l2 = range(size)
a1 = np.arange(size)
a2 = np.arange(size)

start = time.time()
result = [(x,y) for x,y in zip(l1,l2)]
print((time.time()-start)*1000)

start = time.time()
result = a1+a2
print((time.time()-start)*1000)

# In the aboce code i have defined two lists and two numpy arrays.
# Then i compared the time taken in order to find the sum of the lists,
# and the sum of the numpy arrays both. In the output you can see the difference in ms.
