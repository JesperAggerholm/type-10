# HTTP (HyperText Transfer Protocol)

Is an application protocol for distributed, collaborative, hypermedia information systems. HTTP is the foundation of data commuication for the World Wide Web(www),
where hypertext documents include hyperlinks to other resources that the user can easily access, through a web browers.

Developments of HTTP was initiated by Tim Berners-Lee at CERN in 1989. Developments of early HTTP Requests for Comments(RFCs) was a coordinated effort by the internet Engineering Task Force(IETF) and the World Wide
Web Consortium(W3C), with work later moving to IETF.

---

**HTTP/1.1** - https://tools.ietf.org/html/rfc2616
Was firstly documented in RFC *link above* in 1997.

---

**HTTP/2** - https://tools.ietf.org/html/rfc7540
A more efficient expression of the HTTP's semantics "on the wire", and was published in 2015, it's now supported by major web servers and browsers over 
Transport Layer Security(TLS) using Application-Layer Protocol Negotiation(ALPN).

---

**HTTP/3** - https://tools.ietf.org/html/draft-ietf-quic-http-17
Is the proposed successor to HTTP/2 using UDP instead of TCP for the underlying transport protocol. Like HTTP/2, it does not obsolete previous major versions of the protocol.

# HTTPS (HyperText Transfer Protocol Secure)

This is an extension of the HyperText Transfer Protocol(HTTP), it's used for secure communications over computer networks, and is widely used on the internet. 
In HTTPS, the communication protocol is encrypted using Transport Layer Security(TLS), or formerly its predecessor Secure Sockets Layer(SSL). The protocol is therefore
also often referred to as HTTP over TLS, or HTTP over SSL.

The principal motivation for HTTPS is authentication of the accessed website and protection of the privacy and integrity of the exchanged data while in transit. it protects against
man-in-the-middle attacks. The bidirectional interference by attackers with the website that one intended to communicate with, as opposed to an imposter.

# HTTP(S) RFC

**[RFC1945](https://tools.ietf.org/html/rfc1945) - Hypertext Transfer Protocol -- HTTP/1.0**

The Hypertext Transfer Protocol (HTTP) is an application-level
   protocol with the lightness and speed necessary for distributed,
   collaborative, hypermedia information systems. It is a generic,
   stateless, object-oriented protocol which can be used for many tasks,
   such as name servers and distributed object management systems,
   through extension of its request methods (commands). A feature of
   HTTP is the typing of data representation, allowing systems to be
   built independently of the data being transferred.

   HTTP has been in use by the World-Wide Web global information
   initiative since 1990. This specification reflects common usage of
   the protocol referred to as "HTTP/1.0".
 
**[RFC2068](https://tools.ietf.org/html/rfc2068) - Hypertext Transfer Protocol -- HTTP/1.1**

The Hypertext Transfer Protocol (HTTP) is an application-level
protocol for distributed, collaborative, hypermedia information
systems. It is a generic, stateless, object-oriented protocol which
can be used for many tasks, such as name servers and distributed
object management systems, through extension of its request methods.
A feature of HTTP is the typing and negotiation of data
representation, allowing systems to be built independently of the
data being transferred.

HTTP has been in use by the World-Wide Web global information
initiative since 1990. This specification defines the protocol
referred to as "HTTP/1.1".

**[RFC2069](https://tools.ietf.org/html/rfc2069) - An Extenstion to HTTP : Digest Access Authentication**

The protocol referred to as "HTTP/1.0" includes the specification for
   a Basic Access Authentication scheme.  This scheme is not considered
   to be a secure method of user authentication, as the user name and
   password are passed over the network as clear text.  A specification
   for a different authentication scheme is needed to address this
   severe limitation.  This document provides specification for such a
   scheme, referred to as "Digest Access Authentication".  Like Basic,
   Digest access authentication verifies that both parties to a
   communication know a shared secret (a password); unlike Basic, this
   verification can be done without sending the password in the clear,
   which is Basic's biggest weakness. As with most other authentication
   protocols, the greatest sources of risks are usually found not in the
   core protocol itself but in policies and procedures surrounding its
   use.


**[RFC2109](https://tools.ietf.org/html/rfc2109) – HTTP State Management Mechanism**

This document specifies a way to create a stateful session with HTTP
   requests and responses.  It describes two new headers, Cookie and
   Set-Cookie, which carry state information between participating
   origin servers and user agents.  The method described here differs
   from Netscape's Cookie proposal, but it can interoperate with
   HTTP/1.0 user agents that use Netscape's method.  (See the HISTORICAL
   section.)

**[RFC2145](https://tools.ietf.org/html/rfc2145) – Use and Interpretation of HTTP Version Numbers**

HTTP request and response messages include an HTTP protocol version
   number.  Some confusion exists concerning the proper use and
   interpretation of HTTP version numbers, and concerning
   interoperability of HTTP implementations of different protocol
   versions.  This document is an attempt to clarify the situation.  It
   is not a modification of the intended meaning of the existing
   HTTP/1.0 and HTTP/1.1 documents, but it does describe the intention
   of the authors of those documents, and can be considered definitive
   when there is any ambiguity in those documents concerning HTTP
   version numbers, for all versions of HTTP.

**[RFC2169](https://tools.ietf.org/html/rfc2169) – A Trivial Convention for using HTTP in URN Resolution**

The Uniform Resource Names Working Group (URN-WG) was formed to
   specify persistent, location-independent names for network accessible
   resources, as well as resolution mechanisms to retrieve the resources
   given such a name. At this time the URN-WG is considering one
   particular resolution mechanism, the NAPTR proposal [1]. That
   proposal specifies how a client may find a "resolver" for a URN. A
   resolver is a database that can provide information about the
   resource identified by a URN, such as the resource's location, a
   bibliographic description, or even the resource itself. The protocol
   used for the client to communicate with the resolver is not specified
   in the NAPTR proposal.  Instead, the NAPTR resource record provides a
   field that indicates the "resolution protocol" and "resolution
   service requests" offered by the resolver.

   This document specifies the "THTTP" resolution protocol - a trivial
   convention for encoding resolution service requests and responses as
   HTTP 1.0 or 1.1 requests and responses.  The primary goal of THTTP is
   to be simple to implement so that existing HTTP servers may easily
   add support for URN resolution. We expect that the databases used by
   early resolvers will be useful when more sophisticated resolution
   protocols are developed later.

**[RFC2227](https://tools.ietf.org/html/rfc2227) – Simple Hit-Metering and Usage-Limiting for HTTP**

This document proposes a simple extension to HTTP, using a new
   "Meter" header, which permits a limited form of demographic
   information (colloquially called "hit-counts") to be reported by
   caches to origin servers, in a more efficient manner than the
   "cache-busting" techniques currently used.  It also permits an origin
   server to control the number of times a cache uses a cached response,
   and outlines a technique that origin servers can use to capture
   referral information without "cache-busting."

**[RFC2295](https://tools.ietf.org/html/rfc2295) – Transparent Content Negotiation in HTTP**

 HTTP allows web site authors to put multiple versions of the same
   information under a single URL.  Transparent content negotiation is
   an extensible negotiation mechanism, layered on top of HTTP, for
   automatically selecting the best version when the URL is accessed.
   This enables the smooth deployment of new web data formats and markup
   tags.

**[RFC2296](https://tools.ietf.org/html/rfc2296) – HTTP Remote Variant Selection Algorithm**

 HTTP allows web site authors to put multiple versions of the same
   information under a single URL.  Transparent content negotiation is a
   mechanism for automatically selecting the best version when the URL
   is accessed.  A remote variant selection algorithm can be used to
   speed up the transparent negotiation process. This document defines
   the remote variant selection algorithm with the version number 1.0.

**[RFC2518](https://tools.ietf.org/html/rfc2518) – HTTP Extensions for Distributed Authoring**

This document specifies a set of methods, headers, and content-types
   ancillary to HTTP/1.1 for the management of resource properties,
   creation and management of resource collections, namespace
   manipulation, and resource locking (collision avoidance).

**[RFC2585](https://tools.ietf.org/html/rfc2585) – Internet x.509 Public Key Infrastructure Operational Protocols**

The protocol conventions described in this document satisfy some of
   the operational requirements of the Internet Public Key
   Infrastructure (PKI).  This document specifies the conventions for
   using the File Transfer Protocol (FTP) and the Hypertext Transfer
   Protocol (HTTP) to obtain certificates and certificate revocation
   lists (CRLs) from PKI repositories.  Additional mechanisms addressing
   PKIX operational requirements are specified in separate documents.

**[RFC2616](https://tools.ietf.org/html/rfc2616) – Hypertext Transfer Protocol**

The Hypertext Transfer Protocol (HTTP) is an application-level
   protocol for distributed, collaborative, hypermedia information
   systems. It is a generic, stateless, protocol which can be used for
   many tasks beyond its use for hypertext, such as name servers and
   distributed object management systems, through extension of its
   request methods, error codes and headers [47]. A feature of HTTP is
   the typing and negotiation of data representation, allowing systems
   to be built independently of the data being transferred.

   HTTP has been in use by the World-Wide Web global information
   initiative since 1990. This specification defines the protocol
   referred to as "HTTP/1.1", and is an update to RFC 2068 [33].

**[RFC2617](https://tools.ietf.org/html/rfc2617) – HTTP Authentication**

"HTTP/1.0", includes the specification for a Basic Access
   Authentication scheme. This scheme is not considered to be a secure
   method of user authentication (unless used in conjunction with some
   external secure system such as SSL [5]), as the user name and
   password are passed over the network as cleartext.

   This document also provides the specification for HTTP's
   authentication framework, the original Basic authentication scheme
   and a scheme based on cryptographic hashes, referred to as "Digest
   Access Authentication".  It is therefore also intended to serve as a
   replacement for RFC 2069 [6].  Some optional elements specified by
   RFC 2069 have been removed from this specification due to problems
   found since its publication; other new elements have been added for
   compatibility, those new elements have been made optional, but are
   strongly recommended.

**[RFC2774](https://tools.ietf.org/html/rfc2774) – An HTTP Extention Framework**

A wide range of applications have proposed various extensions of the
   HTTP protocol. Current efforts span an enormous range, including
   distributed authoring, collaboration, printing, and remote procedure
   call mechanisms. These HTTP extensions are not coordinated, since
   there has been no standard framework for defining extensions and
   thus, separation of concerns. This document describes a generic
   extension mechanism for HTTP, which is designed to address the
   tension between private agreement and public specification and to
   accommodate extension of applications using HTTP clients, servers,
   and proxies.  The proposal associates each extension with a globally
   unique identifier, and uses HTTP header fields to carry the extension
   identifier and related information between the parties involved in
   the extended communication.

**[RFC2817](https://tools.ietf.org/html/rfc2817) – Upgrading to TLS within HTTP**

This memo explains how to use the Upgrade mechanism in HTTP/1.1 to
   initiate Transport Layer Security (TLS) over an existing TCP
   connection. This allows unsecured and secured HTTP traffic to share
   the same well known port (in this case, http: at 80 rather than
   https: at 443). It also enables "virtual hosting", so a single HTTP +
   TLS server can disambiguate traffic intended for several hostnames at
   a single IP address.

   Since HTTP/1.1 [1] defines Upgrade as a hop-by-hop mechanism, this
   memo also documents the HTTP CONNECT method for establishing end-to-
   end tunnels across HTTP proxies. Finally, this memo establishes new
   IANA registries for public HTTP status codes, as well as public or
   private Upgrade product tokens.

   This memo does NOT affect the current definition of the 'https' URI
   scheme, which already defines a separate namespace
   (http://example.org/ and https://example.org/ are not equivalent).

**[RFC2818](http://tools.ietf.org/html/2818) – HTTP Over TLS**

This memo describes how to use TLS to secure HTTP connections over
   the Internet. Current practice is to layer HTTP over SSL (the
   predecessor to TLS), distinguishing secured traffic from insecure
   traffic by the use of a different server port. This document
   documents that practice using TLS. A companion document describes a
   method for using HTTP/TLS over the same port as normal HTTP
   [RFC2817].

**[RFC2935](https://tools.ietf.org/html/rfc2935) – Internet Open Trading Protocol**

Internet Open Trading Protocol (IOTP) messages will be carried as
   Extensible Markup Language (XML) documents.  As such, the goal of
   mapping to the transport layer is to ensure that the underlying XML
   documents are carried successfully between the various parties.

   This document describes that mapping for the Hyper Text Transport
   Protocol (HTTP), Versions 1.0 and 1.1.

**[RFC2936](https://tools.ietf.org/html/rfc2936) – HTTP MIME Type Handler Detection**

Entities composing web pages to provide services over the Hypertext
   Transfer Protocol (HTTP) frequently have the problem of not knowing
   what Multipurpose Internet Mail Extensions (MIME) types have handlers
   installed at a user's browser.  For example, whether an Internet Open
   Trading Protocol (IOTP) or VRML or SET or some streaming media
   handler is available.  In some cases they would want to display
   different web pages or content depending on a MIME handler's
   availability.  This document summarizes reasonable techniques to
   solve this problem for most of the browsers actually deployed on the
   Internet as of early 2000.  It is intended to be of practical use to
   implementors during the period before the wide deployment of superior
   standards based techniques which may be developed.

**[RFC2964](https://tools.ietf.org/html/rfc2964) – Use of http State Management**

The mechanisms described in "HTTP State Management Mechanism" (RFC-
   2965), and its predecessor (RFC-2109), can be used for many different
   purposes.  However, some current and potential uses of the protocol
   are controversial because they have significant user privacy and
   security implications.  This memo identifies specific uses of
   Hypertext Transfer Protocol (HTTP) State Management protocol which
   are either (a) not recommended by the IETF, or (b) believed to be
   harmful, and discouraged.  This memo also details additional privacy
   considerations which are not covered by the HTTP State Management
   protocol specification.

**[RFC2965](https://tools.ietf.org/html/rfc2965) – HTTP State Management Mechanism**

This document specifies a way to create a stateful session with
   Hypertext Transfer Protocol (HTTP) requests and responses.  It
   describes three new headers, Cookie, Cookie2, and Set-Cookie2, which
   carry state information between participating origin servers and user
   agents.  The method described here differs from Netscape's Cookie
   proposal [Netscape], but it can interoperate with HTTP/1.0 user
   agents that use Netscape's method.  (See the HISTORICAL section.)

   This document reflects implementation experience with RFC 2109 and
   obsoletes it.

**[RFC3143](https://tools.ietf.org/html/rfc3143) – Known HTTP Proxy/Caching Problems**

This document catalogs a number of known problems with World Wide Web
   (WWW) (caching) proxies and cache servers.  The goal of the document
   is to provide a discussion of the problems and proposed workarounds,
   and ultimately to improve conditions by illustrating problems.  The
   construction of this document is a joint effort of the Web caching
   community.

**[RFC3205](https://tools.ietf.org/html/rfc3205) – On the use of HTTP as a Substrate**

Recently there has been widespread interest in using Hypertext
   Transfer Protocol (HTTP) as a substrate for other applications-level
   protocols.  This document recommends technical particulars of such
   use, including use of default ports, URL schemes, and HTTP security
   mechanisms.

**[RFC3229](https://tools.ietf.org/html/rfc3229) – Delta encoding in HTTP**

This document describes how delta encoding can be supported as a
   compatible extension to HTTP/1.1.

   Many HTTP (Hypertext Transport Protocol) requests cause the retrieval
   of slightly modified instances of resources for which the client
   already has a cache entry.  Research has shown that such modifying
   updates are frequent, and that the modifications are typically much
   smaller than the actual entity.  In such cases, HTTP would make more
   efficient use of network bandwidth if it could transfer a minimal
   description of the changes, rather than the entire new instance of
   the resource.  This is called "delta encoding."


**[RFC3230](https://tools.ietf.org/html/rfc3230) – Instance Digests in HTTP**

HTTP/1.1 defines a Content-MD5 header that allows a server to include
   a digest of the response body.  However, this is specifically defined
   to cover the body of the actual message, not the contents of the full
   file (which might be quite different, if the response is a Content-
   Range, or uses a delta encoding).  Also, the Content-MD5 is limited
   to one specific digest algorithm; other algorithms, such as SHA-1
   (Secure Hash Standard), may be more appropriate in some
   circumstances.  Finally, HTTP/1.1 provides no explicit mechanism by
   which a client may request a digest.  This document proposes HTTP
   extensions that solve these problems.

**[RFC3310](https://tools.ietf.org/html/rfc3310) – Hypertext Transfer Protocol (HTTP) Digest Auth using Auth and Key Agreement** 

This memo specifies an Authentication and Key Agreement (AKA) based
   one-time password generation mechanism for Hypertext Transfer
   Protocol (HTTP) Digest access authentication.  The HTTP
   Authentication Framework includes two authentication schemes: Basic
   and Digest.  Both schemes employ a shared secret based mechanism for
   access authentication.  The AKA mechanism performs user
   authentication and session key distribution in Universal Mobile
   Telecommunications System (UMTS) networks.  AKA is a challenge-
   response based mechanism that uses symmetric cryptography.

**[RFC4130](https://tools.ietf.org/html/rfc4130) – MIME-Based Secure Peer-to-Peer Business Data Interchange Using HTTP**

This document provides an applicability statement (RFC 2026, Section 
   3.2) that describes how to exchange structured business data securely
   using the HTTP transfer protocol, instead of SMTP; the applicability
   statement for SMTP is found in RFC 3335.  Structured business data
   may be XML; Electronic Data Interchange (EDI) in either the American
   National Standards Committee (ANSI) X12 format or the UN Electronic
   Data Interchange for Administration, Commerce, and Transport
   (UN/EDIFACT) format; or other structured data formats.  The data is
   packaged using standard MIME structures.  Authentication and data
   confidentiality are obtained by using Cryptographic Message Syntax
   with S/MIME security body parts.  Authenticated acknowledgements make
   use of multipart/signed Message Disposition Notification (MDN)
   responses to the original HTTP message.  This applicability statement
   is informally referred to as "AS2" because it is the second
   applicability statement, produced after "AS1", RFC 3335.

**[RFC4169](https://tools.ietf.org/html/rfc4169) – Hypertext Transfer Protocol**

HTTP Digest, as specified in RFC 2617, is known to be vulnerable to
   man-in-the-middle attacks if the client fails to authenticate the
   server in TLS, or if the same passwords are used for authentication
   in some other context without TLS.  This is a general problem that
   exists not just with HTTP Digest, but also with other IETF protocols
   that use tunneled authentication.  This document specifies version 2
   of the HTTP Digest AKA algorithm (RFC 3310).  This algorithm can be
   implemented in a way that it is resistant to the man-in-the-middle
   attack.


**[RFC4229](https://tools.ietf.org/html/rfc4229) – HTTP Header Field Registrations**

This document defines the initial contents of a permanent IANA
   registry for HTTP header fields and a provisional repository for HTTP
   header fields, per RFC 3864.

**[RFC4236](https://tools.ietf.org/html/rfc4236) – HTTP Adaptation with Open Pluggable Edge Services**

Open Pluggable Edge Services (OPES) framework documents several
   application-agnostic mechanisms such as OPES tracing, OPES bypass,
   and OPES callout protocol.  This document extends those generic
   mechanisms for Hypertext Transfer Protocol (HTTP) adaptation.
   Together, application-agnostic OPES documents and this HTTP profile
   constitute a complete specification for HTTP adaptation with OPES.


**[RFC4387](https://tools.ietf.org/html/rfc4387) – Certificate Store Access Via HTTP**

The protocol conventions described in this document satisfy some of
   the operational requirements of the Internet Public Key
   Infrastructure (PKI).  This document specifies the conventions for
   using the Hypertext Transfer Protocol (HTTP/HTTPS) as an interface
   mechanism to obtain certificates and certificate revocation lists
   (CRLs) from PKI repositories.  Additional mechanisms addressing PKIX
   operational requirements are specified in separate documents.

**[RFC4559](https://tools.ietf.org/html/rfc4559) – SPNEGO-Based Kerberos and NTLM HTTP Authentication in Microsoft**

This document describes how the Microsoft Internet Explorer (MSIE)
   and Internet Information Services (IIS) incorporated in Microsoft
   Windows 2000 use Kerberos for security enhancements of web
   transactions.  The Hypertext Transport Protocol (HTTP) auth-scheme of
   "negotiate" is defined here; when the negotiation results in the
   selection of Kerberos, the security services of authentication and,
   optionally, impersonation (the IIS server assumes the windows
   identity of the principal that has been authenticated) are performed.
   This document explains how HTTP authentication utilizes the Simple
   and Protected GSS-API Negotiation mechanism.  Details of Simple And
   Protected Negotiate (SPNEGO) implementation are not provided in this
   document.

**[RFC4918](https://tools.ietf.org/html/rfc4918) - HTTP Extensions for Web Distribution Authoring and Versioning** 

Web Distributed Authoring and Versioning (WebDAV) consists of a set
   of methods, headers, and content-types ancillary to HTTP/1.1 for the
   management of resource properties, creation and management of
   resource collections, URL namespace manipulation, and resource
   locking (collision avoidance).

   RFC 2518 was published in February 1999, and this specification
   obsoletes RFC 2518 with minor revisions mostly due to
   interoperability experience.

**[RFC5789](https://tools.ietf.org/html/rfc5789) – Patch Method for HTTP**

 Several applications extending the Hypertext Transfer Protocol (HTTP)
   require a feature to do partial resource modification.  The existing
   HTTP PUT method only allows a complete replacement of a document.
   This proposal adds a new HTTP method, PATCH, to modify an existing
   HTTP resource.


**[RFC5843](https://tools.ietf.org/html/rfc5843) – Additional Hash Algorithms for HTTP Instance Digests**

The IANA registry named "Hypertext Transfer Protocol (HTTP) Digest
   Algorithm Values" defines values for digest algorithms used by
   Instance Digests in HTTP.  Instance Digests in HTTP provide a digest,
   also known as a checksum or hash, of an entire representation of the
   current state of a resource.  This document adds new values to the
   registry and updates previous values.


**[RFC5861](https://tools.ietf.org/html/rfc5861) – HTTP Cache-Control Extensions for Stale Content**

This document defines two independent HTTP Cache-Control extensions
   that allow control over the use of stale responses by caches.

**[RFC5985](https://tools.ietf.org/html/rfc5985) – HTTP-Enabled Location Delivery (HELD)**

This document defines a Layer 7 Location Configuration Protocol (L7
   LCP) and describes the use of HTTP and HTTP/TLS as transports for the
   L7 LCP.  The L7 LCP is used for retrieving location information from
   a server within an access network.  It includes options for
   retrieving location information in two forms: by value and by
   reference.  The protocol is an extensible application-layer protocol
   that is independent of the session layer.

**[RFC5987](https://tools.ietf.org/html/rfc5987) – Character Set and Language Encoding for HTTP**

 By default, message header field parameters in Hypertext Transfer
   Protocol (HTTP) messages cannot carry characters outside the ISO-
   8859-1 character set.  RFC 2231 defines an encoding mechanism for use
   in Multipurpose Internet Mail Extensions (MIME) headers.  This
   document specifies an encoding suitable for use in HTTP header fields
   that is compatible with a profile of the encoding defined in RFC
   2231.

**[RFC5989](https://tools.ietf.org/html/rfc5989) – A SIP Event Package for Subscribing to Change to an HTTP Resource**

The Session Initiation Protocol (SIP) is increasingly being used in
   systems that are tightly coupled with Hypertext Transport Protocol
   (HTTP) servers for a variety of reasons.  In many of these cases,
   applications can benefit from being able to discover, in near real-
   time, when a specific HTTP resource is created, changed, or deleted.
   This document proposes a mechanism, based on the SIP Event Framework,
   for doing so.


**[RFC6155](https://tools.ietf.org/html/rfc6155) – Use of Device Identity in HTTP-Enable Location Delivery (HELD)**

When a Location Information Server receives a request for location
   information (using the locationRequest message), described in the
   base HTTP-Enabled Location Delivery (HELD) specification, it uses the
   source IP address of the arriving message as a pointer to the
   location determination process.  This is sufficient in environments
   where the location of a Device can be determined based on its IP
   address.

   Two additional use cases are addressed by this document.  In the
   first, location configuration requires additional or alternative
   identifiers from the source IP address provided in the request.  In
   the second, an entity other than the Device requests the location of
   the Device.

   This document extends the HELD protocol to allow the location request
   message to carry Device identifiers.  Privacy and security
   considerations describe the conditions where requests containing
   identifiers are permitted.
   
**[RFC6202](https://tools.ietf.org/html/rfc6202) – Long Polling and Streaming in Bidirectional HTTP**

On today's Internet, the Hypertext Transfer Protocol (HTTP) is often
   used (some would say abused) to enable asynchronous, "server-
   initiated" communication from a server to a client as well as
   communication from a client to a server.  This document describes
   known issues and best practices related to such "bidirectional HTTP"
   applications, focusing on the two most common mechanisms: HTTP long
   polling and HTTP streaming.

**[RFC6249](https://tools.ietf.org/html/rfc6249) – Metalink/HTTP: Mirrors and Hashes**

This document specifies Metalink/HTTP: Mirrors and Cryptographic
   Hashes in HTTP header fields, a different way to get information that
   is usually contained in the Metalink XML-based download description
   format.  Metalink/HTTP describes multiple download locations
   (mirrors), Peer-to-Peer, cryptographic hashes, digital signatures,
   and other information using existing standards for HTTP header
   fields.  Metalink clients can use this information to make file
   transfers more robust and reliable.  Normative requirements for
   Metalink/HTTP clients and servers are described here.

**[RFC6265](https://tools.ietf.org/html/rfc6265) – HTTP State Management Mechanism** 

This document defines the HTTP Cookie and Set-Cookie header fields.
   These header fields can be used by HTTP servers to store state
   (called cookies) at HTTP user agents, letting the servers maintain a
   stateful session over the mostly stateless HTTP protocol.  Although
   cookies have many historical infelicities that degrade their security
   and privacy, the Cookie and Set-Cookie header fields are widely used
   on the Internet.  This document obsoletes RFC 2965.

**[RFC6266](https://tools.ietf.org/html/rfc6266) – Use of the Content-Disposition Header Field in HTTP**

 RFC 2616 defines the Content-Disposition response header field, but
   points out that it is not part of the HTTP/1.1 Standard.  This
   specification takes over the definition and registration of Content-
   Disposition, as used in HTTP, and clarifies internationalization
   aspects.

**[RFC6546](https://tools.ietf.org/html/rfc6546) – Transport of Real-time Inter-network Defense (RID) Messages over HTTP/TLS**

The Incident Object Description Exchange Format (IODEF) defines a
   common XML format for document exchange, and Real-time Inter-network
   Defense (RID) defines extensions to IODEF intended for the
   cooperative handling of security incidents within consortia of
   network operators and enterprises.  This document specifies an
   application-layer protocol for RID based upon the passing of RID
   messages over HTTP/TLS.

**[RFC6585](https://tools.ietf.org/html/rfc6585) – Additional HTTP Status Codes**

This document specifies additional HyperText Transfer Protocol (HTTP)
   status codes for a variety of common situations.

**[RFC6712](https://tools.ietf.org/html/rfc6712) – HTTP Transfer for the CMP**

This document describes how to layer the Certificate Management
   Protocol (CMP) over HTTP.  It is the "CMPtrans" document referenced
   in RFC 4210; therefore, this document updates the reference given
   therein.

**[RFC6753](https://tools.ietf.org/html/rfc6753) – A Location Dereference Protocol Using HTTP-Enabled Location Delivered**

This document describes how to use the Hypertext Transfer Protocol
   (HTTP) over Transport Layer Security (TLS) as a dereference protocol
   to resolve a reference to a Presence Information Data Format Location
   Object (PIDF-LO).  This document assumes that a Location Recipient
   possesses a URI that can be used in conjunction with the HTTP-Enabled
   Location Delivery (HELD) protocol to request the location of the
   Target.

**[RFC6797](http://tools.ietf.org/html/6797) – HTTP Strict Transport Security (HSTS)**

This specification defines a mechanism enabling web sites to declare
   themselves accessible only via secure connections and/or for users to
   be able to direct their user agent(s) to interact with given sites
   only over secure connections.  This overall policy is referred to as
   HTTP Strict Transport Security (HSTS).  The policy is declared by web
   sites via the Strict-Transport-Security HTTP response header field
   and/or by other means, such as user agent configuration, for example.

**[RFC6896](http://tools.ietf.org/html/6896) – SCS: KoanLogic’s Secure Cookie Sessions for HTTP**

This memo defines a generic URI and HTTP-header-friendly envelope for
   carrying symmetrically encrypted, authenticated, and origin-
   timestamped tokens.  It also describes one possible usage of such
   tokens via a simple protocol based on HTTP cookies.

   Secure Cookie Session (SCS) use cases cover a wide spectrum of
   applications, ranging from distribution of authorized content via
   HTTP (e.g., with out-of-band signed URIs) to securing browser
   sessions with diskless embedded devices (e.g., Small Office, Home
   Office (SOHO) routers) or web servers with high availability or load-
   balancing requirements that may want to delegate the handling of the
   application state to clients instead of using shared storage or
   forced peering.
   
**[RFC6915](http://tools.ietf.org/html/6915) – Flow Identity Extension for HTTP-Enable Location Delivery (HELD)**

RFC 6155 specifies an extension for the HTTP-Enabled Location
   Delivery (HELD) protocol, allowing the use of an IP address and port
   number to request a Device location based on an individual packet
   flow.

   However, certain kinds of NAT require that identifiers for both ends
   of the packet flow must be specified in order to unambiguously
   satisfy the location request.

   This document specifies an XML Schema and a URN Sub-Namespace for a
   Flow Identity Extension for HELD to support this requirement.

   This document updates RFC 6155 by deprecating the port number
   elements specified therein.

**[RFC6983](https://tools.ietf.org/html/rfc6983) – Models for HTTP-Adaptive-Streaming-Aware CDNI**

This document presents thoughts on the potential impact of supporting
   HTTP Adaptive Streaming (HAS) technologies in Content Distribution
   Network Interconnection (CDNI) scenarios.  The intent is to present
   the authors' analysis of the CDNI-HAS problem space and discuss
   different options put forward by the authors (and by others during
   informal discussions) on how to deal with HAS in the context of CDNI.
   This document has been used as input information during the CDNI
   working group process for making a decision regarding support for
   HAS.

**[RFC7034](http://tools.ietf.org/html/7034) – HTTP Header Field X-Frame-Options**

To improve the protection of web applications against clickjacking,
   this document describes the X-Frame-Options HTTP header field, which
   declares a policy, communicated from the server to the client
   browser, regarding whether the browser may display the transmitted
   content in frames that are part of other web pages.

**[RFC7089](http://tools.ietf.org/html/7089) – HTTP Framework for Time-Based Access to Resource States**

The HTTP-based Memento framework bridges the present and past Web.
   It facilitates obtaining representations of prior states of a given
   resource by introducing datetime negotiation and TimeMaps.  Datetime
   negotiation is a variation on content negotiation that leverages the
   given resource's URI and a user agent's preferred datetime.  TimeMaps
   are lists that enumerate URIs of resources that encapsulate prior
   states of the given resource.  The framework also facilitates
   recognizing a resource that encapsulates a frozen prior state of
   another resource.
   
**[RFC7230](https://tools.ietf.org/html/rfc7230) – HTTP Message Syntax and Routing**

The Hypertext Transfer Protocol (HTTP) is a stateless application-
   level protocol for distributed, collaborative, hypertext information
   systems.  This document provides an overview of HTTP architecture and
   its associated terminology, defines the "http" and "https" Uniform
   Resource Identifier (URI) schemes, defines the HTTP/1.1 message
   syntax and parsing requirements, and describes related security
   concerns for implementations.

**[RFC7231](https://tools.ietf.org/html/rfc7231) – HTTP Semantics and Content**

The Hypertext Transfer Protocol (HTTP) is a stateless application-
   level protocol for distributed, collaborative, hypertext information
   systems.  This document defines the semantics of HTTP/1.1 messages,
   as expressed by request methods, request header fields, response
   status codes, and response header fields, along with the payload of
   messages (metadata and body content) and mechanisms for content
   negotiation.
   
**[RFC7232](https://tools.ietf.org/html/rfc7232) – HTTP Conditional Requests**

The Hypertext Transfer Protocol (HTTP) is a stateless application-
   level protocol for distributed, collaborative, hypertext information
   systems.  This document defines HTTP/1.1 conditional requests,
   including metadata header fields for indicating state changes,
   request header fields for making preconditions on such state, and
   rules for constructing the responses to a conditional request when
   one or more preconditions evaluate to false.
   
**[RFC7233](https://tools.ietf.org/html/rfc7233) – HTTP Range Requests**

The Hypertext Transfer Protocol (HTTP) is a stateless application-
   level protocol for distributed, collaborative, hypertext information
   systems.  This document defines range requests and the rules for
   constructing and combining responses to those requests.
   
**[RFC7234](https://tools.ietf.org/html/rfc7234) – HTTP Caching. R. Fielding**

The Hypertext Transfer Protocol (HTTP) is a stateless application-
   level protocol for distributed, collaborative, hypertext information
   systems.  This document defines HTTP caches and the associated header
   fields that control cache behavior or indicate cacheable response
   messages.
   
**[RFC7235](https://tools.ietf.org/html/rfc7235) – HTTP Authentication**

The Hypertext Transfer Protocol (HTTP) is a stateless application-
   level protocol for distributed, collaborative, hypermedia information
   systems.  This document defines the HTTP Authentication framework.
   
**[RFC7236](http://tools.ietf.org/html/7236) – Initial HTTP Auth Scheme Registrations**

This document registers Hypertext Transfer Protocol (HTTP)
   authentication schemes that have been defined in RFCs before the IANA
   HTTP Authentication Scheme Registry was established.

**[RFC7237](https://tools.ietf.org/html/rfc7237) – Initial HTTP Method Registrations**

This document registers those Hypertext Transfer Protocol (HTTP)
   methods that have been defined in RFCs before the IANA HTTP Method
   Registry was established.
   
**[RFC7239](https://tools.ietf.org/html/rfc7239) – Forwarded HTTP Extension**

This document defines an HTTP extension header field that allows
   proxy components to disclose information lost in the proxying
   process, for example, the originating IP address of a request or IP
   address of the proxy on the user-agent-facing interface.  In a path
   of proxying components, this makes it possible to arrange it so that
   each subsequent component will have access to, for example, all IP
   addresses used in the chain of proxied HTTP requests.

   This document also specifies guidelines for a proxy administrator to
   anonymize the origin of a request.
   
**[RFC7240](https://tools.ietf.org/html/rfc7240) – Prefer Header for HTTP**

This specification defines an HTTP header field that can be used by a
   client to request that certain behaviors be employed by a server
   while processing a request.

**[RFC7469](https://tools.ietf.org/html/rfc7469) – Public Key Pinning Extension for HTTP**

This document defines a new HTTP header that allows web host
   operators to instruct user agents to remember ("pin") the hosts'
   cryptographic identities over a period of time.  During that time,
   user agents (UAs) will require that the host presents a certificate
   chain including at least one Subject Public Key Info structure whose
   fingerprint matches one of the pinned fingerprints for that host.  By
   effectively reducing the number of trusted authorities who can
   authenticate the domain during the lifetime of the pin, pinning may
   reduce the incidence of man-in-the-middle attacks due to compromised
   Certification Authorities.
   
**[RFC7472](https://tools.ietf.org/html/rfc7472) – (IPP) over HTTPS Transport Binding and the ‘ipps’ URI Scheme**

This document defines the Internet Printing Protocol (IPP) over HTTPS
   transport binding and the corresponding 'ipps' URI scheme, which is
   used to designate the access to the network location of a secure IPP
   print service or a network resource managed by such a service.

   This document defines an alternate IPP transport binding to that
   defined in the original IPP URL Scheme (RFC 3510), but this document
   does not update or obsolete RFC 3510.

   This document updates RFCs 2910 and 2911.

**[RFC7480](https://tools.ietf.org/html/rfc7480) – HTTP Usage in the RDAP**

This document is one of a collection that together describes the
   Registration Data Access Protocol (RDAP).  It describes how RDAP is
   transported using the Hypertext Transfer Protocol (HTTP).  RDAP is a
   successor protocol to the very old WHOIS protocol.  The purpose of
   this document is to clarify the use of standard HTTP mechanisms for
   this application.
   
**[RFC7486](https://tools.ietf.org/html/rfc7486) – HTTP Origin-Bound Auth (HOBA)**

HTTP Origin-Bound Authentication (HOBA) is a digital-signature-based
   design for an HTTP authentication method.  The design can also be
   used in JavaScript-based authentication embedded in HTML.  HOBA is an
   alternative to HTTP authentication schemes that require passwords and
   therefore avoids all problems related to passwords, such as leakage
   of server-side password databases.
   
**[RFC7540](https://tools.ietf.org/html/rfc7540) – HTTP Version 2**

This specification describes an optimized expression of the semantics
   of the Hypertext Transfer Protocol (HTTP), referred to as HTTP
   version 2 (HTTP/2).  HTTP/2 enables a more efficient use of network
   resources and a reduced perception of latency by introducing header
   field compression and allowing multiple concurrent exchanges on the
   same connection.  It also introduces unsolicited push of
   representations from servers to clients.

   This specification is an alternative to, but does not obsolete, the
   HTTP/1.1 message syntax.  HTTP's existing semantics remain unchanged.
   
**[RFC7541](http://tools.ietf.org/html/7541) – HPACK: Header Compression for HTTP/2**

This specification defines HPACK, a compression format for
   efficiently representing HTTP header fields, to be used in HTTP/2.
   
**[RFC7615](https://tools.ietf.org/html/rfc7615) – HTTP Auth-info and Proxy-Auth-Info Response Header Fields**

This specification defines the "Authentication-Info" and "Proxy-
   Authentication-Info" response header fields for use in Hypertext
   Transfer Protocol (HTTP) authentication schemes that need to return
   information once the client's authentication credentials have been
   accepted.

**[RFC7616](https://tools.ietf.org/html/rfc7616) – HTTP Digest Access Auth**

The Hypertext Transfer Protocol (HTTP) provides a simple challenge-
   response authentication mechanism that may be used by a server to
   challenge a client request and by a client to provide authentication
   information.  This document defines the HTTP Digest Authentication
   scheme that can be used with the HTTP authentication mechanism.
   
**[RFC7617](https://tools.ietf.org/html/rfc7617) – The ‘Basic’ HTTP Auth Scheme**

This document defines the "Basic" Hypertext Transfer Protocol (HTTP)
   authentication scheme, which transmits credentials as user-id/
   password pairs, encoded using Base64.
   
**[RFC7639](https://tools.ietf.org/html/rfc7639) – The ALPN HTTP Header Field**

 This specification allows HTTP CONNECT requests to indicate what
   protocol is intended to be used within the tunnel once established,
   using the ALPN header field.
   
**[RFC7694](https://tools.ietf.org/html/rfc7694) – HTTP Client-Initiated Content-Encoding**

In HTTP, content codings allow for payload encodings such as for
   compression or integrity checks.  In particular, the "gzip" content
   coding is widely used for payload data sent in response messages.

   Content codings can be used in request messages as well; however,
   discoverability is not on par with response messages.  This document
   extends the HTTP "Accept-Encoding" header field for use in responses,
   to indicate the content codings that are supported in requests.
   
**[RFC7711](https://tools.ietf.org/html/rfc7711) – PKIX over Secure HTTP (POSH)**

 Experience has shown that it is difficult to deploy proper PKIX
   certificates for Transport Layer Security (TLS) in multi-tenanted
   environments.  As a result, domains hosted in such environments often
   deploy applications using certificates that identify the hosting
   service, not the hosted domain.  Such deployments force end users and
   peer services to accept a certificate with an improper identifier,
   resulting in degraded security.  This document defines methods that
   make it easier to deploy certificates for proper server identity
   checking in non-HTTP application protocols.  Although these methods
   were developed for use in the Extensible Messaging and Presence
   Protocol (XMPP) as a Domain Name Association (DNA) prooftype, they
   might also be usable in other non-HTTP application protocols.
   
**[RFC7725](https://tools.ietf.org/html/rfc7725) – An HTTP Status Code to Report Legal Obstacles**

This document specifies a Hypertext Transfer Protocol (HTTP) status
   code for use when resource access is denied as a consequence of legal
   demands.

**[RFC7804](https://tools.ietf.org/html/rfc7804) – Salted Challenge Response HTTP Auth Mechanism**

This specification describes a family of HTTP authentication
   mechanisms called the Salted Challenge Response Authentication
   Mechanism (SCRAM), which provides a more robust authentication
   mechanism than a plaintext password protected by Transport Layer
   Security (TLS) and avoids the deployment obstacles presented by
   earlier TLS-protected challenge response authentication mechanisms.

**[RFC7807](https://tools.ietf.org/html/rfc7807) – Problem Details for HTTP APIs**

This document defines a "problem detail" as a way to carry machine-
   readable details of errors in a HTTP response to avoid the need to
   define new error response formats for HTTP APIs.
   
**[RFC7838](https://tools.ietf.org/html/rfc7838) – HTTP Alternative Services**

This document specifies "Alternative Services" for HTTP, which allow
   an origin's resources to be authoritatively available at a separate
   network location, possibly accessed with a different protocol
   configuration.
   
**[RFC7840](https://tools.ietf.org/html/rfc7840) – A Routing Requests Extension for the HTTP-Enabled Location Delivery Protocol**

 For cases where location servers have access to emergency routing
   information, they are able to return routing information with the
   location information if the location request includes a request for
   the desired routing information.  This document specifies an
   extension to the HTTP-Enabled Location Delivery (HELD) protocol that
   updates RFC 5985 to support this function.  Allowing location and
   routing information to be acquired in a single request response
   exchange updates RFC 6881, as current location acquisition and route
   determination procedures are separate operations.

**[RFC8030](https://tools.ietf.org/html/rfc8030) – Generic Event Delivery Using HTTP Push.**

This document describes a simple protocol for the delivery of real-
   time events to user agents.  This scheme uses HTTP/2 server push.
   
**[RFC8053](https://tools.ietf.org/html/rfc8053) – HTTP Auth Extensions for Interactive Clients**

This document specifies extensions for the HTTP authentication
   framework for interactive clients.  Currently, fundamental features
   of HTTP-level authentication are insufficient for complex
   requirements of various Web-based applications.  This forces these
   applications to implement their own authentication frameworks by
   means such as HTML forms, which becomes one of the hurdles against
   introducing secure authentication mechanisms handled jointly by
   servers and user agents.  The extended framework fills gaps between
   Web application requirements and HTTP authentication provisions to
   solve the above problems, while maintaining compatibility with
   existing Web and non-Web uses of HTTP authentication.
   
**[RFC8075](https://tools.ietf.org/html/rfc8075) – Guidelines for Mapping Implementations: HTTP to CoAP**

This document provides reference information for implementing a
   cross-protocol network proxy that performs translation from the HTTP
   protocol to the Constrained Application Protocol (CoAP).  This will
   enable an HTTP client to access resources on a CoAP server through
   the proxy.  This document describes how an HTTP request is mapped to
   a CoAP request and how a CoAP response is mapped back to an HTTP
   response.  This includes guidelines for status code, URI, and media
   type mappings, as well as additional interworking advice.
   
**[RFC8120](https://tools.ietf.org/html/rfc8120) – Mutual Auth Protocol for HTTP**

This document specifies an authentication scheme for the Hypertext
   Transfer Protocol (HTTP) that is referred to as either the Mutual
   authentication scheme or the Mutual authentication protocol.  This
   scheme provides true mutual authentication between an HTTP client and
   an HTTP server using password-based authentication.  Unlike the Basic
   and Digest authentication schemes, the Mutual authentication scheme
   specified in this document assures the user that the server truly
   knows the user's encrypted password.

**[RFC8121](https://tools.ietf.org/html/rfc8121) – Mutual Auth Protocol for HTTP**

This document specifies cryptographic algorithms for use with the
   Mutual user authentication method for the Hypertext Transfer Protocol
   (HTTP).
   
**[RFC8164](https://tools.ietf.org/html/rfc8164) – Opportunistic Security for HTTP/2**

 This document describes how "http" URIs can be accessed using
   Transport Layer Security (TLS) and HTTP/2 to mitigate pervasive
   monitoring attacks.  This mechanism not a replacement for "https"
   URIs; it is vulnerable to active attacks.
   
**[RFC8187](https://tools.ietf.org/html/rfc8187) – Indicating Character Encoding and Language for HTTP Header Field Parameters**

By default, header field values in Hypertext Transfer Protocol (HTTP)
   messages cannot easily carry characters outside the US-ASCII coded
   character set.  RFC 2231 defines an encoding mechanism for use in
   parameters inside Multipurpose Internet Mail Extensions (MIME) header
   field values.  This document specifies an encoding suitable for use
   in HTTP header fields that is compatible with a simplified profile of
   the encoding defined in RFC 2231.

   This document obsoletes RFC 5987.

**[RCF8188](https://tools.ietf.org/html/rfc8188) – Encrypted Content-Encoding for HTTP**

This memo introduces a content coding for HTTP that allows message
   payloads to be encrypted.
   
**[RFC8216](https://tools.ietf.org/html/rfc8216) – HTTP Live Streaming**

This document describes a protocol for transferring unbounded streams
   of multimedia data.  It specifies the data format of the files and
   the actions to be taken by the server (sender) and the clients
   (receivers) of the streams.  It describes version 7 of this protocol.
   
**[RFC8246](https://tools.ietf.org/html/rfc8246) – HTTP Immutable Responses**

The immutable HTTP response Cache-Control extension allows servers to
   identify resources that will not be updated during their freshness
   lifetime.  This ensures that a client never needs to revalidate a
   cached fresh resource to be certain it has not been modified.
   
**[RFC8297](https://tools.ietf.org/html/rfc8297) – An HTTP Status Code for Indicating Hints**

This memo introduces an informational HTTP status code that can be
   used to convey hints that help a client make preparations for
   processing the final response.

**[RFC8336](https://tools.ietf.org/html/rfc8336) – The ORIGIN HTTP/2 Frame**

 This document specifies the ORIGIN frame for HTTP/2, to indicate what
   origins are available on a given connection.
   
**[RFC8441](https://tools.ietf.org/html/rfc8441) – Bootstrapping WebSockets with HTTP/2**

This document defines a mechanism for running the WebSocket Protocol
   (RFC 6455) over a single stream of an HTTP/2 connection.
   
**[RFC8470](https://tools.ietf.org/html/rfc8470) – Using Early Data in HTTP**

Using TLS early data creates an exposure to the possibility of a
   replay attack.  This document defines mechanisms that allow clients
   to communicate with servers about HTTP requests that are sent in
   early data.  Techniques are described that use these mechanisms to
   mitigate the risk of replay.

**[RFC8473](https://tools.ietf.org/html/rfc8473) – Token Binding over http**

This document describes a collection of mechanisms that allow HTTP
   servers to cryptographically bind security tokens (such as cookies
   and OAuth tokens) to TLS connections.

   We describe both first-party and federated scenarios.  In a first-
   party scenario, an HTTP server is able to cryptographically bind the
   security tokens that it issues to a client -- and that the client
   subsequently returns to the server -- to the TLS connection between
   the client and the server.  Such bound security tokens are protected
   from misuse, since the server can generally detect if they are
   replayed inappropriately, e.g., over other TLS connections.

   Federated Token Bindings, on the other hand, allow servers to
   cryptographically bind security tokens to a TLS connection that the
   client has with a different server than the one issuing the token.

   This document is a companion document to "The Token Binding Protocol
   Version 1.0" (RFC 8471).
   
**[RFC8484](https://tools.ietf.org/html/rfc8484) – DNS Queries over HTTPS**

This document defines a protocol for sending DNS queries and getting
   DNS responses over HTTPS.  Each DNS query-response pair is mapped
   into an HTTP exchange.
   
**[RFC8594](https://tools.ietf.org/html/rfc8594) – The Sunset HTTP Header Field**

This specification defines the Sunset HTTP response header field,
   which indicates that a URI is likely to become unresponsive at a
   specified point in the future.  It also defines a sunset link
   relation type that allows linking to resources providing information
   about an upcoming resource or service sunset.



