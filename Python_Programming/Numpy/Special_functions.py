# Python numpy special functions.
# there are varous special functions avaible in numpy, such as sine, cos, tan, log etc.
# first, lets begin with sine function where we will learn to plot its graph. For what we need to import
# a module called matplotlib.

import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0,3*np.pi,0.1)
y = np.sin(x)
#plt.plot(x,y)
#plt.show()

# trying with the tan.

x = np.arange(0,3*np.pi,0.1)
y = np.tan(x)
#plt.plot(x,y)
#plt.show()

# Moving forward with python numpy, lets see some other special functionality in numpy array such as exponential and logarithmic function.
# now in exponential, the e values is somewhere equal to 2.7 and in log, it's actually log base 10.
# when we talk aabout natural log i.e log base e, it's referred as Ln. so lets see how its implemented.

a = np.array([1,2,3])
print(np.exp(a))

# as you can see above the output, the exponential values are printed i.e e raise to the power of 1 is e, whitch gives the result as 2.718
# similarly, e raises to the power of 2 gives the value somewhere near 7.38 and so on. Next in order to calculate log.

a = np.array([(1,2,3)])
print(np.log(a))

# here we have calculated natural log whitch gives the value as displayed above. Now, if we want log base 10 instead of Ln or natural log, use code below.

a = np.array([(1,2,3)])
print(np.log10(a))

# by this, we can come to the end og this numpy lesson
