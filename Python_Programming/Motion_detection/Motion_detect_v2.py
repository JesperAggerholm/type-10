import cv2, pandas, time, datetime
first_frame = None
status_list = [None, None]
video = cv2.VideoCapture(0)
times = []
# Using Pand dataframe to store the time values during whitch object detections
# and movement appears.

df = pandas.DataFrame(columns=["Start","End"])

while True:
    check, frame = video.read()
    # Status at the beginning of the recording is zero as the object is not visible.
    status = 0
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray,(21,21),0)

    if first_frame is None:
        first_frame = gray
        continue
    # Calculates the difference between the first frame and the others.
    delta_frame = cv2.absdiff(first_frame, gray)
    # Provides a threshold value, such that it will vonvers the difference value with,
    # less then 30 to black. If the difference is greater than 30 it will convert those pixels to white.
    thresh_delta = cv2.threshold(delta_frame, 30, 255, cv2.THRESH_BINARY)[1]
    thresh_delta = cv2.dilate(thresh_delta, None, iterations=0)
    # Defines the contour area. Basically, adding the borders.
    (cnts,_) = cv2.findContours(thresh_delta.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    for contour in cnts:
        if cv2.contourArea(contour) < 1000:
            continue
        # Cahnging the status when the object is being detected.
        status = 1
        (x,y,w,h) = cv2.boundingRect(contour)
        cv2.rectangle(frame, (x,y), (x + w , y + h), (0,255,0),3)

    # List of status for every frame.
    status_list.append(status)
    status_list = status_list[-2:]

    # Recording datetime in a list when a change occurs.
    if status_list[-1] == 1 and status_list[-2] == 0:
        times.append(datetime.datetime.now())
    if status_list[-1] == 1 and status_list[-2] == 1:
        times.append(datetime.datetime.now())

    cv2.imshow('frame', frame)
    #cv2.imshow('capturing', gray)
    #cv2.imshow('delta', delta_frame)
    #cv2.imshow('thresh', thresh_delta)

    key = cv2.waitKey(1)

    if key == ord('q'):
        if status == 1:
            times.append(datetime.datetime.now())
        break

print(status_list)
print(times)

for i in range(0, len(times),2):
        df=df.append({"start":times[i],"End":times[i+1]},ignore_index = True)

df.to_csv("Python_Programming/Motion_detection/Times.csv")

video.release()
cv2.destroyAllWindows()
