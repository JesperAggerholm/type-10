from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def index():
    name="Sam"
    age = 94
    return render_template('index.html', name=name, age=age)


if __name__ == "__main__":
    app.run()
