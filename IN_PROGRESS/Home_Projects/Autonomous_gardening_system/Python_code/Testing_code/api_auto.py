from flask import Flask, request
from flask_restful import reqparse, abort, Api, Resource
from flask_cors import CORS, cross_origin
from atmega import *

application = Flask(__name__)
CORS(application, resources=r'/*') # to allow external resources to fetch data
api = Api(application)

@api.resource("/")
class url_index(Resource):
    def get(self):
        returnMessage = {"message": "Yes, it works"}
        return returnMessage

@api.resource("/pumpauto")
class url_auto(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = auto(ser)
        return returnMessage

@api.resource("/pumpon")
class url_on(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = pump_on(ser)
        return returnMessage

@api.resource("/pumpoff")
class url_off(Resource):
    def get(self):
        with connect("/dev/ttyS0", 9600) as ser:
            returnMessage = pump_off(ser)
        return returnMessage

if __name__ == '__main__':
    application.run(host="0.0.0.0", debug=True)
