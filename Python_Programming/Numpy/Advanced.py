# ndim finding the dimension of the array whetever its two dimensional,
# array or a singel dimensional array, below we are going to use it pratcially how to findind
# the dimentions, with the help of the ndim function. That can find whether the array is of single dimention or multidimention.

import numpy as np

a = np.array([(1,2,3),(4,5,6)])
print(a.ndim)

# the output will display a 2, as this is a two-dimentional array.
# * ---------------------------------------------------------------------------------------*
# itemsize can be used to calculate the byte size of each element in the array.
a = np.array([(1,2,3)])
print(a.itemsize)

# output is 4, so every elememt occupies 4 byte in the above numpy array.
# * ---------------------------------------------------------------------------------------*
# dtype you can find the data type of the elements that are stored in an array. So, if you want to know the data type of a particular element.
# you can use the 'dtype' function whitch will print the datatype along with the size.

a = np.array([(1,2,3)])
print(a.dtype)

# The output shows the datatype is 32 bits integer. similarly, we can find the size and shape of the array using 'size' and 'shape' function respectively.
a = np.array([(1,2,3,4,5,6)])
print(a.size)
print(a.shape)

# * ---------------------------------------------------------------------------------------*
# reshape is when tou change the number of rows and columms whitch gives a new view to an object. Now let us make an example to reshape the below array.

a = np.array([(8,9,10),(11,12,13)])
print(a)
a = a.reshape(3,2)
print(a)

# slicing the 'reshape' function, showed us how to rearrage the array.
# Now lets take another operation, slicing. this is basically extracting particular set of elements from an array.
# we need a particular element say 3, out of the given array.

a = np.array([(1,2,3,4),(3,4,5,6)])
print(a[0,2])

# here the array (1,2,3,4) is your index 0 and (3,4,5,6) is index 1 of the python numpy array.
# therefore we have printed the second element form the zeroth index.
# taking one step forward, lets say we need the 2nd element from the zeroth index of the array.

a = np.array([(1,2,3,4),(43,4,5,6)])
print(a[0:,2])

# here colon represents all te rows, including 0. now to get the 2nd element, we'll call index 2 from both
# of the rows whitch gives us the vlaues 3 and 5 respectively.
# next just remove the confusion, lets say we have one or more ro and we dont want to get its 2nd element printed just as the image above.
# what we can do in such a case.

a = np.array([(8,9),(10,11),(12,13)])
print(a[0:2,1])

#as you can see in the above code, only 9 and 11 gets printed. Now when i have written 0:2, whis does
# not include the second index of the third row of an array, therefore only 0 and 11 gets printed else you will get all the elements.
# * ---------------------------------------------------------------------------------------*
# linspace this is another operation in python numpy that returns evenly spaced numbers over a specified interval.

a = np.linspace(1,3,10)
print(a)

# as you can see the result is printed 10 values between 1 to 3.
# * ---------------------------------------------------------------------------------------*
# max/min we have some operations in numpy such as to find the maxium and the miium as well as the sum of the numpy array.

a = np.array([(1,2,3)])
print(a.min())
print(a.max())
print(a.sum())

#the concept of axis in python numpy. suppose you want to calculate the sum of all the solumns
# then you can make use of the axis.

a = np.array([(1,2,3),(3,4,5)])
print(a.sum(axis=0))

# the output is 4 6 8, therefore the sum of all the columns are added where 1+3=4, 2+4=6 and 3+5=8
# * ---------------------------------------------------------------------------------------*
# square root and te standard diviation, there are varous mathematical functions that can be performed using python numpy.
# you can find the square root, standard deviation of the array.

a = np.array([(1,2,3),(4,5,6)])
print(np.sqrt(a))
print(np.std(a))
# * ---------------------------------------------------------------------------------------*
# addition operation, you can perform more operations on numpy array addition, substraction, multiplication and division of the two matrices.

x = np.array([(1,2,3),(3,4,5)])
y = np.array([(1,2,3),(3,4,5)])
print(x+y)

x = np.array([(1,2,3),(3,4,5)])
y = np.array([(1,2,3),(3,4,5)])
print(x+y)
print(x-y)
print(x*y)

# * ---------------------------------------------------------------------------------------*
# ravel os one more operation, where you can convert one numpy array into a single column i.e ravel.

x = np.array([(1,2,3),(3,4,5)])
print(x.ravel())

# the output is a ravled array.
# * ---------------------------------------------------------------------------------------*
