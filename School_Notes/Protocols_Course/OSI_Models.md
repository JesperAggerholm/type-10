# OSI model

The *Open Systems Interconnection model(OSI model)* a conceptual framework that describes the functions of a networking or telecommunications system.
The model uses layers to help give a visual description of what is going on with a particular networking system. *very usefull for troubleshooting*.

![alt text](https://miro.medium.com/max/1024/1*17Zz6v0HWIzgiOzQYmO6lA.jpeg "OSI Model")

## Layer 7 - Application

The Application Layer is at the top, being what most users see. In the [OSI model](https://en.wikipedia.org/wiki/OSI_model "OSI WIKI"), this is the layer that is the "closest to the end user". Application that work at layer 7 are the ones that users interract with directly. A web browser(Google chrome, Firefox, Safari, etc) or other applications, like skype, Discord, Office, Outlook.

## Layer 6 - Presentation

The Presentation Layer, represents independent data in preparations or translation for application format to a network format, or the opposite way.
A good example would be encryption and decryption of data for secure transmission.

## Layer 5 - Session

When devices such as a computer and a server, need to communicate with one another, a session will be created, this is done in the session layer. 
functions in this layer involve setup, coordination(How long the system wais for a response ex.) and termination between the application at each end of the session.

## Layer 4 - Transport

The Transport layer deals with the coordination of the data transfer between end system hosts. How much data that needs to be sent, at what rate, where it goes, etc. 
The best known example of the transport layer is the *Transmission Control Protocol(TCP)* that is built on top of the *Internet Protocol(IP)* commenly know as *TCP/IP*.
TCP and UDP port numbers work at layer 4, while IP addresses work at layer 3.

## Layer 3 - Network

The network Layer is where you find most of the router functionality that most networking professionals care about. 
This layer is responsible for packet forwarding, including routing through different routers. 

## Layer 2 - Data Link

the Data Link provides node-to-node data transfer, and also handles error corrections from tje physical layer. Two sublayers exists here as well- the *Media Acces Control(MAC)* layer and the *Logical Link Control(LLC)* Layer. Most switches work at this layer.

## Layer 1 - Physical 

The Physical Layer, represents the electrial and physiscal representation of the system. This includes everything from cable type, radio frequency link(as in an 802.11 wireless systems), as well as the layout of pins, voltages and other physical requirements.

#### Reference


