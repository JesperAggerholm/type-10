# RESTful API

## Description

The goal of this section is to build with Python3, Flask and Connextion a REST API. <br />
That can include input and output validation, and provide some usefull documentation for either personal use or my fellow students.

# What is REST

Rest or REpresentational State Transfer, is an architetural style for providing standards between computer systems on the weeb, <br />
making it easier to communicate with each other. REST-compliant systems often called RESTful systems, are characterized <br />
by how they are stateless and seperate the concerns of client and server. 

## Seperation of client and server 

REST architectural style, the implementation of the client and the impletementation of the server can be done <br />
independetly without each knowing about the other. This means the code of the client side can be changed at any <br /> 
time without affecting the operation of the server, and the code on the server side can be changed without affecting <br />
the operation of the client.

As long as each side knows the format of the message to send to the other, they can be kept modular and seperate.
Seperating the user interface concerns from the data storage concerns, will improve flexibility of the interface across 
platforms and improve scalability by simplifying the server components. Additionily, the seperation allows each component the ability to evolve independently. 

## Statelessness

Systems following the REST paradigm are stateless, that means the server does not need to anything about the state of the clients is in and vice versa. <br />
In this way, both the server and the client can understand any message received, even without seeing previous messages. This constraint of statelessnes <br />
is enforced through the use of resourcs, rather than commands Resources are the need to store or send to other services.

Because REST systems interact through standard operations on resources, they do not rely on the implementation of interfeces.

## Communication between client and server

REST architecture, clients send requests to retrive or modify resources, and servers send responses to these requests. 

### Making requests

REST aquires that a client make a request to the sever in order to retrive or modify data on the server. A request  generally consists of:

- An HTTP verb, which defines that kind of operation to perform.
- A header, which allows the client to pass along information about the request.
- A path to a resource.
- an opetional message body containing data.

### HTTP verbs

There are 4 basic HTTP verbs we can use in requests to interact with resources in a REST system:

- *GET* - Retrive a specific resource (BY ID) or a collection of resources.
- *POST* - Create a new resource
- *PUT* - update a specific resource (BY ID)
- *DELETE* - Remove a specific resource (BY ID)

### Headers and accept parameters

In the header of the request, the client sends te type of the content that it is able to receive from the server. <br />
This is called the accept field, and it ensires that the server does not send the data that cannot be understood or processed by the client. <br />
The options for the types of content are MIME Types *(Multipurpose Internet Main Extensions)*

MIME Types, used to specifi the content types in the accept field, consists of a type and a subtype. The are seperated by slash (/).


# REFERENCE LIST!

https://www.codecademy.com/articles/what-is-rest
