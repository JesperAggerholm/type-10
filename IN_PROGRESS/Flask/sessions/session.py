from flask import *

app = Flask(__name__)

#setting the secret ket to some random bytes.
app.secret_key = b'Uranium235'

@app.route('/')
def index():
    if 'username' in session:
        return 'Logged in as %s' % escape(session['username'])
    return 'You are not logged in'

@app.route('/login', methods = ['GET', 'POST'])
def login():
    if request.method == 'POST':
        session['username'] = request.form['username']
        return redirect[url_for('index')]
    return '''
    <form method="post">
    <p><input> type="text" name="username">
    <p><input> type="submit" value="Login">
    </form>
    '''

@app.route('/logout')
def logout():
    #remove username from the session.
    session.pop('username', None)
    return redirect(url_for('index'))
