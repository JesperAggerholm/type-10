import serial
import time
import yaml

conf_file = "C:/SPB_Data/type-10/Home_Projects/Autonomous_gardening_system/Python_code/Testing_code/conf.yml"

if __name__ == "__main__":
        with open(conf_file, 'r') as stream:
            serial_cfg = yaml.safe_load(stream)

        print("Running on port {}".format(serial_cfg['device']))

        with serial.Serial(serial_cfg['device'], serial_cfg['baud'], timeout =1) as ser :
            try:
                print("Running simple connection - with test parameters")
                time.sleep(3)

                print("PUMP - ON")
                ser.write("2".encode())
                reply = ser.readline().decode()
                print("- {}".format(reply))

                print("zzzz")
                time.sleep(2)

                print("PUMP - OFF")
                ser.write("1".encode())
                reply = ser.readline().decode()
                print("- {}".format(reply))

                print("zzzz")
                time.sleep(2)

                print("AUTO - ON/OFF")
                ser.write("3".encode())
                reply = ser.readline().decode()
                print(reply)

            except KeyboardInterrupt:
                print('Interruptet!')
