# WebDAV

Web Distributed Authoring and Versioning (WebDAV) is an extension of the Hypertext Transfer Protocol (HTTP) that allows clients to perform remote Web content authoring operations. 
WebDAV is defined in https://tools.ietf.org/html/rfc4918 by a working group of the Internet Engineering Task Force.

The WebDAV1 protocol provides a framework for users to create, change and move documents on a server. 
The most important features of the WebDAV protocol include the maintenance of properties about an author or modification date, namespace management, collections, and overwrite protection. Maintenance of properties includes such things as the creation, removal, and querying of file information. Namespace management deals with the ability to copy and move web pages within a server's namespace. Collections deal with the creation, removal, and listing of various resources. Lastly, overwrite protection handles aspects related to locking of files.

Many modern operating systems provide built-in client-side support for WebDAV.