### 1.1 Collision domains and broadcast domains.

A ***Collision domain*** is a part of a network where packet collisions can occur. A collisions occurs when two devices sends packets at the same time on the shared network segment. When collisions occur, the devices will send the packets again, therefore reduces network efficiency.

Collisions are often in a hub environment, because each port on a hub is in the same collision domain.

A ***Broadcast domain*** is a domain where a broadcast is forwarded. It contains all devices that can reach each other on at the data link layer(layer 2 OSI) by using broadcast.

All ports on a switch or hub, is by default in the same broadcast domain. All ports on a router is in different broadcast domains and routers don't forward broadcasts from one domain to another.

### 1.2 Function of routers and switches.

***Router*** layer 3 device, that works on network layer of the OSI model, that connects two different networks and it identifies network devices based on their IP addresses.

the routers are used for connecting a local network to the other local network/s. They are generelly located at the gateway where two or more networks connect.

***Switch*** layer 2 device that works on the data link layer(OSI layer 2), it communicates by using frames and it identifies network devices based on MAC addresses or physical addresses.

***System Switching Board*** (SSB) and Forwarding Engine Board(FEB) are the two different router models used for the control board functionality.

An ***aggregate route*** is the second form of a locally configured route within the JUNOS software.

### 1.3 Optical network fundamentals-SONET/SDH, OTN.

-TBD- / Not needed USA standards.

### 1.4 Ethernet Networks.

Ethernet is a layer 2 technology, that operates in a shared bus topology. Ethernet supports broadcast transmission, uses both best-effort delivery, and distributed access control. Ethernet is a point-to-multipoint technology.

In a shared bus topology, all devices connect to a single, shared physical link through which all data transmissions are sent. all traffic is broadcast. The devices within a single ethernet topology make up a broadcast domain.

Ethernet uses best-effort delivery to broadcast traffic.

***Ethernet Access Control and Transmission***

Ethernet uses carrier-sense multiple access with collision detection (CSMA/CD). Because multiple devices on an ethernet network can access the physical medium, or wire, simultaneously, each device must determine if a message is being transmitted. If it detects no transmission, the host begins transmitting it's own data. The length of the transmission is determined by fixed ethernet packet sizes.

***Collisions and detection***

When a device on an Ethernet network begins transmitting data, the data takes a finite amount of time to reach all hosts on the network. Because of this delay, or latency, in transmitting traffic, a device might detect an idle state on the wire just as another device initially begins its transmission. As a result, two devices might send traffic across a single wire at the same time. When the two electrical signals collide, they become scrambled so that both transmissions are effectively lost.

To handle collisions, Ethernet devices monitor the link while they are transmitting data. The monitoring process is known as collision detection, if a device detects a foreign signal while it's transmitting, it terminates the transmission and attempts to transmit again only after detecting an idle state on the wire. Collisions continue to occur if two colliding devices both wait the same amount of time before re-transmitting. To avoid this condition, Ethernet devices use a binary exponential back off algorithm.

### 1.5 Layer2 addressing and address resolution.

The Internet is a global, public network with IP subnets connected by routers and exchanging packets. Both Ethernet and IP use globally unique network addresses that can be used as the basis for a truly global network. Ethernet MAC addresses come from the IEEE and IP subnet addresses come from various internet authorities.

- MAC addresses are 48 bits long. The first 24 bits are assigned by the IEEE and form the organizationally unique identifier (OUI) of the manufacturer or vendor requesting the address. The last 24 bits from the serial number of the LAN interfaces cards and their uniqueness must me enforced by the company (some companies reuse numbers of bad or returned cards while others do not).

- IPv4 addresses are 32 bits long. A variable number of the beginning bits are assigned by an Internet authority and represents a subnet located somewhere in the world. The remaining bits are assigned locally and, when joined to the network portion of the address, uniquely identify some host on a particular network.

- IPv6 addresses are 128 bits long.

***Address Resolution*** ARP is the Address Resolution Protocol, which maintains a table named as ARP table. Sending IP packets on a multi access network requires mapping from an IP address to a media access control (MAC) address (the physical or hardware address).

In an Ethernet environment, ARP is used to map a MAc address to an IP address. ARP dynamically bing the IP address (the logical address) to the correct MAC address. Before IP unicast packets can be sent, ARP discovers the MAC address used bu the Ethernet interface where the IP address is configured.

### 1.6 IPv4 and IPv6 Fundamentals.

IP addresses are assigned by the central numbering authority called the interned assigned numbers authority (IANA). IANA ensures that addresses are globally unique where needed and has a large address space reserved for use by devices not visible outside their own networks.

***IPv4 addressing:***

IPv4 addresses are 32-bit numbers that are typically displayed in dotted decimal notation. A 32 bit address contains two primary parts: ***Network prefix*** and the ***Host number***. All hosts in a single network, share the same network address. Each host has an address that uniquely identifies it. Depending on the scope of the network and the type of device, the address is either globally or locally unique. Visible devices to users outside the network (like ***Webservers***) must have a globally unique IP address.

***IPv4 classful addressing***

To provide flexibility in the number of addresses distributed to networks of different sizes, 4-octet (32-bit) IP addresses were originally divided into three different categories or classes: class A, class B and class C.

- class A addresses use only the first bite(octet) to specify the network prefix. leaving 3 bytes to define individual host numbers.
- class B addresses use the first 2 bytes. leaving 2 bytes.
- class C addresses use the first 3 bytes. leaving 1 byte, to identify hosts.

in binary. x representing each bit in the host number.

- 00000000 xxxxxxxx xxxxxxxx xxxxxxxx (class A) Natural mask: 255.0.0.0
- 00000000 00000000 xxxxxxxx xxxxxxxx (class B) Natural mask: 255.255.0.0
- 00000000 00000000 00000000 xxxxxxxx (class C) Natural mask: 255.255.255.0

***Subnetting***

subnetting an IP network is done primarily for better utilization of available IP address space, and routing purpose.

***Subnetwork Mask format***

IPv4 subnetwork masks specified in one of the two ways: dotted decimal or prefix length notation. Dotted decimal notation expresses IP addresses and masks in dotted quads - four octets separated by dots - (A,B,C,D) in this format, each octet in the address or mask is represented as a decimal number and the dots are used as octet seperator.

Prefix length notation (network prefix format) allows for more efficient allocation of IP addresses than the old Class A, B and C address scheme.

### 1.7 Binary Numbers to Decimal Number Conversion and Vice Versa.

-This can be looked up on the internet.

### 1.8 Longest match routing.

Specific the static route on the device to resolve and determine the packets next-hop interface using the Longest Match Routing Rule, sometimes referred to as the longest prefix match or maximum prefix length match. It's an algorithm used by IP  routers to select an entry from a routing table. The routers uses the longest prefix match to determine the egress(outbound) interface and the address of the next device to which to send a packet.

The router implements the Longest Match Routing Rule as follows:

- The router receives a packet
- While processing the header, the router compared the destination IP address bit-by-bit, while entries in the routing table.
- The entry that has the longest number of network bits that match the IP destination address is always the best match (or path)

### 1.9 Connection Oriented and Connections protocols.

- Should know this, will fill out later.

### 2.1 Junos device Portfolio.

The operating system software that powers the Juniper routers is called JUNOS. The software is modular and standards are based. Another important feature of JUNOS is that the software is platform independent (within the juniper hardware systems, not to be confused with other vendor hardware), thus delivering the same scalability and security across several hardware platforms.

***JTAC*** refers to Juniper Networks Technical Assistance Center. JTAC is the recommendation body of Juniper Networks that provides suitable guidelines for Juniper devices.

***Main products offered by Juniper Include:***

ROUTERS:

- ***T-series***
- ***M-series***
- ***J-series***
- ***E-series***
- ***MX-series***

SWITCHES:

- ***Ex Series Ethernet switches***
- ***QFX series***

***control and Forwarding plane***

On juniper router there is a separation of control and the forwarding planes. Control plane is where the OS of the device resides. Control plane is also maintains the routing table and the best route to reach a particular destination. Control plane then forwards the best path to the forwarding plance. Then, the forwarding plane forwards the data to the destination.

The forwarding planes enhances the performance using the ASICs whereas the intelligence of the router operates in forwarding plane.

### 2.2 Software Architecture.

The jdocs package contains the complete JUNOS software documentation set. Jbundle is a single file which contains all the packages.
The removable media is the first boot location examined. solid-state drive is the second boot examined after removable media. It takes about 5 minutes to completely boot a junos device.

### 2.3 Routing Engine and Packaet Forwarding Engine.

the routing engine is the central location for control of the system in a juniper networks router and it consists of an intel-based PCI platform running JUNOS software. The routing engine constructs and maintains one or more routing tables. From the routing tables, the routing engine derives a table of active routes, called the forwarding table, which is then copied into the packet forwarding engine.

### 2.4 Protocol DAEMONS.

The actual functions of the router are controlled bu the daemon. Each daemon operate in its own protected memory space, which is also controlled by the kernel. the management daemon process all user acces to the router. For an example, the user's CLI is a client of mgd. The chassis Daemon process controls the properties of the router itself, including the interaction of the passive midplane, the FPCs, and the control boards.

The management Daemon process controls all user access to the router.

For example, the user's CLI is a client of mgd.

the router's interfaces are configured and maintained by the Device Control Daemon.

***Exception Traffic:***  Packets addressed to the router, such as ICMP pings, Telnet, and SSH traffic are exceptional packet.

### 3.1 concepts, operation and functionality of the junos user interface.

***CLI Functionality:*** the Junos CLI (Command Line Interface) is a simple to use, text-based command interface. We give various commands on CLI for configuring, troubleshooting and monitoring the software.

***a) Operational mode:*** When first loggin into the router and the CLI starts, we are at the top level of the CLI operational mode. In this mode, we enter commands for.

1. Controlling the CLI environment.
2. Monitor and troubleshoot network connectivity.
3. initiating the Configuration mode.

Operational mode is indicated by the ***>*** prompt-for

***b) Configuration mode:*** Used for configuring the JUNOS software by creating a hierarchy of configuration statements. This mode is entered by using the ***configure***

configuration mode is indicated by the ***#***

### 3.2 Active vs. Candidate configuration.

***Active configuration:*** In junos os, the new configuration changes that we actually made can be compared with an active configuration what is currently running on our Junos devices. In order to compare the configuration with an active configuration, 'show | compare' command is used in the configuration mode.

***Canditate configuration:*** We can view the candidate configuration that is present on our juniper device from configuration mode hierarchy. Candidate configuration allows us to make configuration changes without causing operational changes to the current operating configuration.

***Configure private:*** Allows to enter into private configuration mode, where every user have their own private candidate configuration. Also when a user commits, it's only the users own changes that's commited.

***Commit synchronize:*** used when we have two routing engines and we need to apply the candidate configuration to both.

### 3.3 Reverting to previous configurations.

We can place the previously configured file in the candidate configuration with the rollback command. To return to a configuration prior to the most recently committed one, include the configuration number, 0 through 49, in the rollback command. The most recently saved configuration is number 0.

### 3.4 Modifying, managing, and saving configuration files.

In order to write the current candidate configuration to the permanent storage, we enter a save command along with the path to locate the candidate configuration. The contents of the current level of the statement hierarchy (and below) are saved, along with the statement hierarchy containing it. This allows a section of the configuration to be saved, while fully specifying the statement hierarchy. By default it's saved to the home directory , on the flash drive.

/var/home is located on the router's hard drive.

### 3.5 J-WEB - core/common functionality.

J-WEB is a GUI (Graphical User Interface) used to configure the junos device. The J-Web interface allows you to monitor, configure, troubleshoot, and manage the routing platform by means of a Web Browser enabled with Hypertext Transfer Protocol (HTTP) or HTTP over Secure Socket Layer (HTTPS)- J-Web provides access to all the configuration statements supported by the routing platform, so you can fully configure it without using Junos OS CLI.

### 4.1 Factory default state.

In, juniper devices there is an option to load our configuration back to the default factory state. The use of ***"Load factory-default"*** command in the configuration mode hierarchy reverts the device to the factory default state. Before it's possible to commit, you have to set ***root-authentication password***.

### 4.2 User authentication methods.

The two authentication methods supported by the JUNOS software are MD5 and simple authentication. Simple authentication uses a plain-text password that is included in the transmitted packet whereas, MD5 does hashing while transmitting the packets.

### 4.3 Initial configuration.

1. Connect a terminal or laptop computer to the router through the console port -a serial port on the front of the router. Only console access to the router is enabled by default.

2. Power on the router.

The Junos OS boots automatically. The boot process is complete when you see the loging: prompt on the console.

3. Login as the user root.

initially, the root user account requires no password. You can see that you are the root user, because the prompt on the router shows username ***root@#***.

4. Start the Junos OS command-line interface (CLI):

***root@#cli***
***root@>***

5. Enter the Junos OS configuration mode:

***cli>configure***
***[edit]***
***root@#***

6. When we use the run command, the router allows us to access the operation mode command by sitting on configuration mode hierarchy.

7. The up command allows us to move one hierarchy upper from the excising hierarchy.

### 4.4 Interface Types and Properties.

-TBD-

### 4.5 Describe how to configure basic components of a Junos device.

-TBD-

### 5.1 Show commands.

***show version brief:*** We use the show version brief command to view the currently running version on the Junos devices. When upgrading our device, we first check the current OS version that is running on our device.

***show route protocol rip:*** shows all the RIP routes learned by a router from it's neighbor routers.

***show interface terse:*** shows the interfaces that are currently installed on a router. Interfaces are always displayed in numerical order, from the lowest to the highest FPC slot number.  

***show interface's filters:*** displays all firewall filters configured on all interfaces on the router. We can only specify a particular interfaces filters by using "show interface filters" command.

***show ospf statistics:*** displays the counter based on the OSPF packet type. Both the total number of packets and the number in the last 5 seconds is shown with this command.

***show chassis hardware:*** issued to verify the hardware contents on our juniper devices.

***show firewall log:*** displays entries in the memory-resident buffer or kernel cache. The router stores information in this uffer when the log filter action is used.

***show ospf neighbor:*** command on the operational mode hierarchy shows the status of our ospf neighbor router.

***show interfaces extensive:*** displays all possible information about every interface currently installed in the router. We have the option of specifying a particular interface.

by default, every IPv6 unicast information are placed in the inet6.0 routing table. we can verify this table using 'show route table inet6.0' command.

RIP protocol is configured on our router from configuration mode hierarchy.

***show ospf route:*** used to verify the OSPF routes in juniper devices. This also displays the state of the learned OSPF network.

***show interfaces:*** on the operational mode hierarchy and 'run show interfaces' command in the configuration mode hierarchy shows the information of all the interfaces on our juniper devices.

***show arp:*** the 'show arp' command displays the entries in the Address Resolution Protocol (ARP) table. This shows only entries for hosts that the router has attempted to send traffic to.

***show route protocol aggregate:*** displays the route learned from other routes configured with aggregated route. By default the aggregate route will appear in the inet.0 routing table when at least one contributing route is in the routing table.

### 5.2 Monitor commands.

The ***'Monitor interface interface-name'*** command displays per-second real-time statistics for a physical interface. We can also view common interface failures, such as alarms, errors, or loopback settings.

***show route table inet.0:*** we use this command in the operational mode, to view IPv4 unicast routes whereas 'show route table inet.1' command displays IPv4 multicast routes.

### 5.3 Network tools.

-TBD-

### 5.4 Root password recovery.

-TBD-

### 6.1 Routing table.

JUNOS OS automatically creates and maintains several routing tables. Each routing table is used for a specific purpose. In addition to these automatically created routing tables, you can create your on routing tables.

Each routing table populates a portion of the forwarding table. Thus, the forwarding table is partitioned bases on routing tables. The allows for specific forwarding behavior for each routing table.

Creating routing tables is optional. If you do not create any junos os uses its default routing tables, which as follows.

inet.0 and inet.2 are the default routing tables used in the Junos device.

the inet.0 routing table is the table used to store IPv4 unicast routes. The router interfaces and all routing protocols place information into this table by default.

### 6.2 Static Routing.

-TBD-

### 6.3 Routing protocols.

***RIP:*** refers to Routing Information Protocol, which is one of the dynamic routing. It receives the response message to trigger an update response sent by a neighbor. Response messages are also received in response to a request message generated by the router and for an unsolicited response message sent by the neighbor.

***OSPF:*** Open Shortest Path First (OSPF) in an open-standard interior gateway (IGP) routing protocol. Unlike other Routing Protocols such as Routing Information Protocol (RIP), Enhanced Interior Gateway Routing Protocol (EIGRP) or Border Gateway Protocol (BGP), OSPF uses the link state algorithm in conjunction with Edsger W. Dijkstra Shortest Path First (SPF) algorithm to send OSPF advertisements, known as link-state advertisements (LSAs), to share its local link-state database (LSDB) witch OSPF enabled devices to create an overall topology of every router, link state metric within a network. OSPF defined in RFC2328:

OSPF is a link-state routing protocol. It's designed to be run internal to a single Autonomous system. Each OSPF router maintains an identical database describing the Autonomous systems topology. From this database, a routing table is calculated by constructing a shortest-path tree.

By default, internal OSPF routes have a preference value of 10, and external OSPF routes 150.

Multi-area OSPF is supported in Juniper devices. It means that we can create two different OSPF areas on the same Autonomous system (AS). But all OSPF networks must be a member of Area 0.

***BGP:*** The Border Gateway Protocol (BGP) is the routing protocol that is extensively used in the internet to connect ISP networks. BGP is a path vector routing protocol. BGP uses route mechanism that is comparable to OSPF or IS-IS. The two of the BGP states used when establishing relationships are Idle and Active. Idle is the initial neighbor state, in which it rejects all incoming session requests. In the Active state, the local router is trying to initiate a TCP session with it's peer.

In BGP networks, 4 messages types are exchanged between two peers. Those 4 message types are Open, Update, Notification, and Keepalive.

### 7.1 Default Routing Policies.

***Routing Policy*** Allows you to control the routing information between the routing protocols and the routing tables and between the routing tables and the forwarding table. All routing protocols use the Junos OS routing tables to store routes that they learn and to determine which routes they should advertise in their protocol packets. Routing policy allows you to control which routes the routing protocols store in and retrieve from the routing table.

***Reasons to create a routing policy***

The following are typical circumstances under which you might want to preempt the default routing policies in the routing policy framework by creating your own routing policies:

- You do not want a protocol to import all routes into the routing table. If the routing table does not learn about certain routes, they can never be used to forward packets and they can never be redistributed into other routing protocols.

- You do not want a routing protocol to export all the active routes it learns.

- You want a routing protocol to export all the active routes it learns.

- You want a routing protocol to announce active routes learned from another routing protocol, which is sometimes called route redistribution.

- You want to manipulate route characteristics, such as the preference value, AS path, or community. You can manipulate the route characteristics to control which route is selected as the active route to reach a destination. In general, the active route is also advertised to a router's neighbors.

- You want to change the default BGP route flap-damping parameters.

- You want to perform per-packet load balancing.

- You want to enable class of service (CoS).

***Policy Components***

All policies are composed of the following components that you configure.

- Match conditions - Criteria against which route or packets are compared. You can configure one or more criteria. If all criteria match, one or more actions are applied.

- Actions - What happens if all criteria match. You can configure one or more actions.

- Terms - Names structures in which match conditions and actions are defined. You can define one or more terms.

The policy framework software evaluates each incoming and outgoing route or packet against match conditions in a term. If the criteria in the match conditions are met, the defined action is taken. In general, the policy framework software compares the route or packet against the match conditions in the first term in the policy, then goes on to the next term, and so on. Therefore, the order in which you arrange term in a policy is relevant. The order of match conditions within a term is not relevant because a route or packet must match all match conditions in a term for an action to be taken.

***Import and export policies:*** Import and export policies controls the view of the local router and the neighbor router. We can configure an import and an export policy under [edit protocols] hierarchy. There is no default import policy for OSPF. But the default export policy for OSPF is to reject all routes.

There are three possible results that each policy contains. Both accept and reject are considered terminating actions and they have a special meaning-they stop the policy evaluation. The next policy clarifies that the route should be evaluated by the next position in the policy chain.

### 7.2 Firewall Filter Concepts.

***Firewall filter policy:*** Allows you to control packets transiting the router to a network destination and packets destined from and sent by the router.

In Junos devices, a firewall filter in router is implemented using internet processor (ASCI). The internet processor builds on the fundamental performance and reliability by adding enhanced security functions, increased visibility into network operations. Each firewall filter JUNOS software contains a hidden term that cause a single final action for all filters. This final action is to discard all packets.

Firewall filters enables to control packets transiting the device to a network destination as well as packets destined for and sent by the device. You can configure a firewall filter to perform specified action on packets of a particular protocol family, including fragmented packets, that match specified conditions based on Layer3 or Layer4 packet header fields.

***Stateless and Stateful Firewall Filters.***

A ***Stateless firewall***, also known as an access control list (ACL), does not statefully inspect traffic. Instead, it evaluates packet contents statically and does not keep track of the state of network connections. Stateless firewalls watch network traffic, and restrict or block packets based on source and destination addresses or other static values. They are not aware of traffic patterns or data flows.

In contras, a ***Stateful firewall filter*** uses connection state information from other applications and past communications in the data flow make dynamic control decisions. stateful firewalls can watch traffic streams from end to end. They are aware of communications paths and implement various IP security (IPsec) functions such as tunnels and encryption. In technical terms, this means that stateful firewalls can tell what state a TCP connection is in
(Open, open sent, synchronized, synchronization acknowledge or established), it can tell is the MTU has changed, whatever packets have fragmented etc.

***Firewall Filter Components***

In a firewall filter, you first define the family address type (ethernet-switching, inet (for IPv4), Inet6 (for IPv6), circuit cross-connect(CCC), or MPLS), and then define one or more terms that specify the filtering criteria and the action to take if a match occurs.

Each term consists of the following components

- Match conditions - Specify values that a packet must contain to be considered a match. You can specify values for most fields in the IP, TCP, UDP, or ICMP headers. You can also match on interfaces names.

- Action - specifies what to do if a packet matches the match conditions. A filter can accept, discard, or reject a matching packet and then perform additional actions, such as counting, classifying, and policing. If no action is specified for a term, the default is to accept the matching packet.

***Firewall Filter Processing:*** If there are multiple terms in a filter, the order of the terms is important. If a packet matches the first term, the switch executes the action defined by that term, and no other terms are evaluated. If the switch does not find a match between the packet and the first term, it compares the packet to the next term. If no match occurs between the packet and the second term, the system continues to compare the packet to each successive term in the filter until a match is found. If the packet does not match any terms in the filter, the switch discards the packet by default.
