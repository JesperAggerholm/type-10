var baseUrl = 'http://192.168.137.174:5000/'

$(document).ready(function(){

    httpGet(baseUrl+'led');
    httpGet(baseUrl+'led');
    httpGet(baseUrl+'led');

    var ctx = document.getElementById("myChart").getContext('2d');
    window.myLine = new Chart(ctx, config);
    
    $(".on-btn").click(() => {
        httpPut(baseUrl+'led', {'value': '1'});      
    });

    $(".off-btn").click(() => {
        httpPut(baseUrl+'led', {'value': '0'});      
    });

    $('#card-switches').children().on('change', function(data){
        var selSwitch = document.getElementById(data.target.id);
        selSwitch.checked ? httpPut(baseUrl + 'led'+selSwitch.id.substr(-2), { 'value': '1' }) : httpPut(baseUrl + 'led'+selSwitch.id.substr(-2), { 'value': '0' });
    });
});

//HTTP GET
function httpGet(url) {
    return fetch(url)  
    .then((response) => {
        var responseClone = response.clone();
        updateLastMessage(response);
        return responseClone;
    })  
    .then((responseClone) => responseClone.json())
    .then((responseData) => {
        updateElement(responseData, url);
    })
    .catch((error) => {
        console.error(error)
        updateLastMessage(error);
    });
}

//HTTP PUT
function httpPut(url, data) {
    return fetch(url, {
        method: 'PUT', // 'GET', 'PUT', 'DELETE', etc.
        body: JSON.stringify(data), // Coordinate the body type with 'Content-Type'
        headers: new Headers({
            'Content-Type': 'application/json;charset=UTF-8'
        }),
    })
    .then((responseData) => {
        httpGet(responseData.url);
        updateLastMessage(responseData);
    })
    .catch((error) => {
        console.error(error)
        updateLastMessage(error);
    });
}

//OUTPUTS
function updateElement(data, url){
    let element = document.getElementById('status-field-'+url.substr(-2)); //element status-field-A0 etc. 
    value = data.value;
    element.innerHTML = value;
    parseInt(value) ? element.style.backgroundColor = "darkolivegreen" : element.style.backgroundColor = "tomato";
}

function updateLastMessage(text)
{
    let time = curTime();
    let li = document.createElement("li");
    li.appendChild(document.createTextNode(`${time} ${text.status} ${text.statusText}`));
    $('#api-log').prepend(li);
}

//CHART CONFIG
var config = {
    type: 'line',
    data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
        datasets: [{
            label: 'temperature',
            fill: false,
            backgroundColor: window.chartColors.red,
            borderColor: window.chartColors.red,
            data: [
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor(),
                randomScalingFactor()
            ]					
        }
        , {
            label: 'humidity',
            fill: false,
            backgroundColor: window.chartColors.blue,
            borderColor: window.chartColors.blue,
            data: [
                15,28,40,32,100,0,-10,100,9,4,5
            ],
        }
    ]
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        title: {
            display: false,
            text: 'Chart.js Line Chart'
        },
        tooltips: {
            mode: 'index',
            intersect: false,
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Month'
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'Value'
                }
            }]
        }
    }
};

function curTime (){
    let now = new Date().toLocaleTimeString('da-DK');
    return now;
}