from jnpr.junos import Device
from jnpr.junos.op.ethport import EthPortTable
from jnpr.junos.op.routes import RouteTable
from jnpr.junos.op.arp import ArpTable
from jnpr.junos.utils.config import Config
from jnpr.junos.exception import *
from lxml import etree
from pprint import pprint
import sys

class Menu:
    global upload
    global menu_splitter
    global dev

    menu_splitter = (30 * "-" , "SRX - MAKESTER" , 30 * "-")

    try:
        Device.auto_probe = 3
        dev = Device(host = '172.25.11.12', user = 'Auto', password = 'lab123')
        print ('Opening connection to ')
        dev.open()
    except Exception as somethingIsWrong:
        print ("Unable to connect to host:", somethingIsWrong)
        sys.exit(1)

    def display_menu(self):
        print(menu_splitter)
        print("""
        Srx configuration and showcasing Menu.

        1. Edit configuration.
        2. Show information.
        3. Troubleshooting.
        4. [Quit]

        """)
        print(menu_splitter)

    def run(self):
        while True:
            self.display_menu()
            selection = input("[Plsease select an option:] ...")

            if selection == 1:
                self.configuration()
            elif selection == 2:
                self.facts()
            elif selection == 3:
                self.troubleshooting()
            elif selection == 4:
                self.quit()
            else:
                print("Not a valid input!")

    def configuration(self):
        while True:
            print(menu_splitter)
            print("""
            Options for configuration of the router.

            1. Show the configuration.
            2. Download configuration.
            3. Upload configuration.
            3. [Go back]

            """)
            print(menu_splitter)

            subchoice_config = input("[Plsease select an option:] ...")

            if subchoice_config == 1:
                print(dev.cli("show configuration", warning=False))
            elif subchoice_config == 2:
                f = open("demofile30.txt", "w")
                f.write(dev.cli("show configuration", warning=False))
                f.close()
            elif subchoice_config == 3:
                dev.open()
            	print("Device Connected")
            	dev.bind(cu=Config) # Bind the Config instance to the Device instance.
            	dev.cu.lock() # Lock the configuration.
            	dev.cu.load(path="demofile30.txt", overwrite=True) # Load configuration to Junos
            	dev.cu.commit() # Commit the configuration on Junos device
            	dev.cu.unlock() # Unlock the configuration
            	print(dev.cli("show system commit",warning=False))
                print("Upload worked")
            elif subchoice_config == 4:
                break
            else:
                print("Not a valid input!")
            continue

    def facts(self):
        while True:
            print(menu_splitter)
            print("""
            Current configuration on the device.

            1. Show facts about the router.
            2. Interface information.
            3. Routing Table.
            4. Arp Table.
            5. [Go back]

            """)
            print(menu_splitter)
            subchoice_config = input("[Plsease select an option:] ... ")

            if subchoice_config == 1:
                pprint(dev.facts)
            elif subchoice_config == 2:
                eths = EthPortTable(dev)
                eths.get()
                for port in eths:
                    print ("{}: {}".format(port.name, port.oper))
            elif subchoice_config == 3:
                print("Printing the active Routing table ... \n" )
                routes = RouteTable(dev)
                routes.get()
                for rt in routes:
                    print("{}".format(rt.name))
            elif subchoice_config == 4:
                print("Printing the active Arp table ... \n" )
                Arp = ArpTable(dev)
                Arp.get()
                for arp_table in Arp:
                    print("{}".format(arp_table.name))
            elif subchoice_config == 5:
                break
            else:
                print("Not a valid input!")
            continue

    def troubleshooting(self):
        while True:
            print(menu_splitter)
            print("""
            Troubleshooting of the device.

            1. Ping.
            2. Traceroute.
            3. [Go back]

            """)
            print(menu_splitter)
            subchoice_config = input("[Plsease select an option:] ...")

            if subchoice_config == 1:
                print("TBD")
            elif subchoice_config == 2:
                print("TBD")
            elif subchoice_config == 3:
                break
            else:
                print("Not a valid input!")
            continue

    def quit(self):
        print("Thank you for using this automated software .. be watching you!")
        sys.exit(0)

if __name__ == "__main__":
    Menu().run()