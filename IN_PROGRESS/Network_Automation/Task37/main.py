#Made By Munksbo
import multiprocessing
from netmiko import Netmiko

def Jobagenet(workerNr,dataset):
    workerinfo=dataset[workerNr]
    net_connect = Netmiko(str(workerinfo[0]),username=str(workerinfo[1]),password=str(workerinfo[2]),device_type="juniper_junos",)
    for i in Work(workerinfo[3]):
        if i.strip()=="Save Config":
            savetofile(workerinfo[0],net_connect.send_command_expect("show config"))
        elif "edit" in i.strip():
            print(net_connect.send_config_set(i[5:]))
        else:
            print(net_connect.send_command_expect(i))
    net_connect.disconnect()

def Work(JobDeskription):
    return JobDeskription.split(",")


def savetofile(name,config):
    file = open(name+".json", "w")
    file.write(config[7:])
    file.close()

def ConfigOP(file):
    file = open(file, "r")

    def serverconfigmaker(file):
        server=""
        user=""
        password=""
        work=""
        candidate=[]

        for line in file:
            if server!="" and user!="" and password!="" and work!="":
                temp=[server[:-1],user[:-1],password[:-1],work[:-1]]
                server=""
                user=""
                password=""
                work=""
                candidate.append(temp)
            if"remote_url" in line:
                server=line.split(":::",1)[1]
            elif"remote_username" in line:
                user=line.split(":::",1)[1]
            elif"remote_passphrase" in line:
                password=line.split(":::",1)[1]
            elif"remote_work" in line:
                work=line.split(":::",1)[1]
            else:
                pass
        return candidate

    return serverconfigmaker(file)

if __name__== "__main__":
    jobs = []
    instrut=ConfigOP("server.cfg")
    for i in range(len(instrut)):
        p = multiprocessing.Process(target=Jobagenet, args=(i,instrut,))
        jobs.append(p)
        p.start()
